{-# LANGUAGE TemplateHaskell #-}

module Client where

import Control.Lens
import Data.ByteString
import Peer.Peer
import Protolude hiding (option)
import System.Random
import Torrent.Taint (PieceLength)

data Client = Client
  { _client_peerId :: PeerId,
    _client_ip :: ByteString,
    _client_port :: Int,
    _sub_piece_length :: PieceLength
  }
  deriving (Show, Eq)

makeLenses ''Client

generatePeerId :: IO PeerId
generatePeerId = PeerId <$> toS . ("-flood-" <>) <$> replicateM 13 (randomRIO ('a', 'z'))
