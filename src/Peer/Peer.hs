{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}

-- |
-- Module      : Peer.Peer
-- Description : Peer type and values
-- Copyright   : (c) Haskell Milano, 2020
-- License     : BSD-3
-- Maintainer  : paolo.veronelli@email.com, @entangled90, @massimo
-- Stability   : experimental
-- Portability : POSIX
module Peer.Peer where

import Bencoding
import qualified Data.ByteString as B
import Generics.SOP.TH
import Protolude
import Text.Printf
import qualified Prelude as PP

newtype PeerId = PeerId {unpeer_id :: ByteString}
  deriving
    ( Show,
      Eq,
      Ord,
      IsString
    )

deriveGeneric ''PeerId

getId :: PeerId -> ByteString
getId (PeerId id) = id

data Peer = Peer
  { peer_ip :: ByteString,
    peer_port :: Integer,
    peer_id :: Maybe PeerId
  }

instance PP.Show Peer where
  show (Peer ip p _id) = toS ip <> ":" <> show p -- <> ", " <> maybe "unknown" (toS . unpeer_id) id

peerIdx :: Peer -> (ByteString, Integer)
peerIdx Peer {..} = (peer_ip, peer_port)

instance Eq Peer where
  (==) = on (==) peerIdx

instance Ord Peer where
  compare = comparing peerIdx

peerIdL :: Functor f => (Maybe PeerId -> f (Maybe PeerId)) -> Peer -> f Peer
peerIdL f (Peer ip port x) = Peer ip port <$> f x

deriveGeneric ''Peer

instance BDecode Peer where
  bdecode = gbdecode $ stripLabel "peer_"

instance BEncode Peer where
  bencode = gbencode $ stripLabel "peer_"

mkPeer4 :: ByteString -> [Peer]
mkPeer4 (B.null -> True) = []
mkPeer4 b =
  let (B.unpack -> xs, rest) = B.splitAt 6 b
   in case xs of
        [w1, w2, w3, w4, p0, p1] -> (:)
          do
            Peer
              do toS $ intercalate "." $ map show [w1, w2, w3, w4]
              do fromIntegral p0 * 256 + fromIntegral p1
              do Nothing
          do mkPeer4 rest
        _ -> []

mkPeer6 :: ByteString -> [Peer]
mkPeer6 (B.null -> True) = []
mkPeer6 b =
  let (splitAt 16 . B.unpack -> (ws, ps), rest) = B.splitAt 6 b
      g (x0 : x1 : xs) = Just (printf "%x" $ x0 * 256 + x1, xs)
      g _ = Nothing
   in case ps of
        [p0, p1] -> (:)
          do
            Peer
              do toS $ intercalate ":" $ unfoldr g ws
              do fromIntegral p0 * 256 + fromIntegral p1
              do Nothing
          do mkPeer4 rest
        _ -> []

-- | referred to a peer
data Peered a = Peered
  { peered_peer :: !PeerId,
    peered_value :: a
  }
  deriving (Functor, Show, Eq)
