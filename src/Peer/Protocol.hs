{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Peer.Protocol where

import Client
import Control.Concurrent.Async
import Control.Concurrent.STM
import Control.Lens as Lens
import Control.Logging as Log
import Control.Monad
import Control.Monad.Catch
import Control.Monad.State.Strict
import Data.Attoparsec.Binary as A
import Data.Attoparsec.ByteString as A
import qualified Data.ByteString as BS
import qualified Data.ByteString.Builder as BS
import qualified Data.Map as M
import qualified Data.Set as S
import Data.Word
import Lib
import Peer.Peer
import Pipes as P
import qualified Pipes.Attoparsec as PA
import Pipes.Concurrent
import qualified Pipes.Network.TCP as TCP
import qualified Pipes.Parse as PP
import qualified Pipes.Prelude as P
import Pipes.Safe
import Protolude hiding (runStateT)
import Torrent.Taint
import Torrent.Info 

-- import qualified Network.Socket as STCP

type Begin = Word32

type Length = Word32

-- | describe the presence of pieces by a peer
data Presence = Presence
  { presence_length :: Length,
    presence_set :: Set TaintIndex
  }
  deriving (Show, Eq)

-- | peer protocol encoding
data Protocol
  = KeepAlive
  | Choke
  | UnChoke
  | Interested
  | NotInterested
  | BitField Presence
  | Have TaintIndex
  | Request TaintIndex Begin Length
  | Piece TaintIndex Begin ByteString
  | Cancel TaintIndex Begin Length
  | Unknown ByteString
  deriving (Show, Eq)

-- | peer protocol parser
parseProtocol :: Parser Protocol
parseProtocol =
  do
    l <- p4
    case l of
      0 -> pure KeepAlive
      _ -> do
        id <- p1 <?> "Command id"
        let failId = fail $ "invalid id " <> show id <> " for length " <> show l
        case l of
          1 -> do
            case id of
              0 -> pure Choke
              1 -> pure UnChoke
              2 -> pure Interested
              3 -> pure NotInterested
              _ -> failId
          5 -> do
            case id of
              4 -> Have <$> ti <?> "Have"
              _ -> failId
          13 -> do
            case id of
              6 -> Request <$> ti <*> p4 <*> p4 <?> "Request"
              8 -> Cancel <$> ti <*> p4 <*> p4 <?> "Cancel"
              _ -> failId
          _ -> do
            case id of
              5 ->
                do
                  bs <- A.take $ fromIntegral l - 1
                  pure $ BitField $ parseBitfield bs
              -- <?> "BitField"
              7 ->
                do
                  Piece <$> ti <*> p4 <*> do
                    A.take (fromIntegral $ l - 9)
                  <?> "Piece"
              _ -> failId
  where
    p4 = A.anyWord32be
    p1 = A.anyWord8
    ti = TaintIndex . fromIntegral <$> p4

-- | bit level map to Presence
parseBitfield :: ByteString -> Presence
parseBitfield b = Presence
  do fromIntegral $ BS.length b
  do foldl' f mempty . zip [0 ..] . BS.unpack $ b
  where
    f s (j, w) = foldl' g s [0 .. 7]
      where
        g s' i
          | testBit w i = S.insert (TaintIndex $ fromIntegral $ j * 8 + i) s'
          | otherwise = s'

-- | bit level map from Presence
renderBitfield :: Presence -> ByteString
renderBitfield (Presence (fromIntegral -> n) s) = BS.pack . map f $ [0 .. n -1]
  where
    f :: Int64 -> Word8
    f j = foldl' g 0 [0 .. 7]
      where
        g x i
          | S.member (TaintIndex $ j * 8 + i) s = setBit x $ fromIntegral i
          | otherwise = x

-- | render peer protocol
renderProtocol :: Protocol -> ByteString
renderProtocol = toS . BS.toLazyByteString . \case
  KeepAlive -> r4 0
  Choke -> r4 1 <> r1 0
  UnChoke -> r4 1 <> r1 1
  Interested -> r4 1 <> r1 2
  NotInterested -> r4 1 <> r1 3
  Have n -> r4 5 <> r1 4 <> ti n
  BitField v@(Presence n _) ->
    r4 (n + 1) <> r1 5 <> BS.byteString (renderBitfield v)
  Request i b o -> r4 13 <> r1 6 <> ti i <> r4 b <> r4 o
  Piece i b v ->
    r4 (fromIntegral $ BS.length v + 9)
      <> r1 7
      <> ti i
      <> r4 b
      <> BS.byteString v
  Cancel i b o -> r4 13 <> r1 8 <> ti i <> r4 b <> r4 o
  r -> panic $ "cannot render this " <> show r
  where
    r4 = BS.word32BE
    r1 = BS.word8
    ti (TaintIndex i) = r4 $ fromIntegral i

-- | name of the peer
newtype Handshaked = Handshaked ByteString

-- | encode the handshake for a torrent and client
renderHandshake :: ByteString -> PeerId -> ByteString
renderHandshake info (PeerId peerId) =
  BS.singleton 19
    <> "BitTorrent protocol"
    <> BS.replicate 8 0
    <> info
    <> peerId

-- | parse the handshake from the peer , in case the id is specified it must match
-- if not it's returned via a Protocol extension ( ??? wtf, TODO: fix this out)
parseHandShake :: Maybe PeerId -> Parser Handshaked
parseHandShake mid = do
  _ <- word8 19
  _ <- string "BitTorrent protocol"
  _ <- A.take 28
  id' <- case mid of
    Nothing -> A.take 20 -- they are different, most of the time
    Just (PeerId id) -> string id
  pure (Handshaked id')

parseAll ::
  forall m.
  MonadIO m =>
  Maybe PeerId ->
  Producer ByteString m () ->
  m (PeerId, Producer Protocol m ())
parseAll peerId socketStream = do
  -- initialPeer <- liftIO $ readTVarIO peer
  (r, p') <- runStateT
    do PA.parse $ parseHandShake $ peerId
    do socketStream
  case r of
    Just (Left e) ->
      liftIO $ fail $ "exhausted:" <> show e
    Just (Right (Handshaked peerID)) -> do
      debug' $ "Handshaked with peerId " <> show peerID
      pure (PeerId peerID, void $ PP.parsed (handleNothing <$> PA.parse parseProtocol) p')
    Nothing ->
      liftIO $ fail "exhausted"

handleNothing :: Maybe (Either a b) -> Either (Maybe a) b
handleNothing = maybe (Left Nothing) (either (Left . Just) Right)

reportException :: (MonadCatch m, MonadIO m) => m () -> m ()
reportException f = catchAll
  f
  do \(SomeException e) -> warn' $ show e

forkSafeEffectLinked:: Effect (SafeT IO) () -> IO ()
forkSafeEffectLinked = liftIO . fork . reportException . runSafeT . runEffect

forkEffectLinked :: MonadIO m => Effect IO () -> m ()
forkEffectLinked = liftIO . fork . runEffectReporting

runEffectReporting :: Effect IO () -> IO ()
runEffectReporting = reportException . runEffect

keepAliveThread :: MonadIO m => Producer Protocol m ()
keepAliveThread = forever $ do
  liftIO $ threadDelay $ 60 * 1_000_000
  yield KeepAlive

-- | connect to a peer
connectToPeer ::
  -- | TCP target
  Peer ->
  -- | torrent information
  TorrentInfo  ->
  -- | client information
  Client ->
  -- | outgoing messages
  Producer Protocol IO () ->
  -- | incoming messages
  Consumer (Peered Protocol) IO () ->
  MVar PeerId ->
  IO ()
connectToPeer initialPeer@(Peer h p _id) TorrentInfo  {..} client' messageOut messageIn peerIdRef = do
  reportException $ TCP.connect (toS h) (show p) $ \(s, _) -> do
    log' $ "Connected to " <> show initialPeer
    race_
      do
        runEffectReporting $
          do
            yield (renderHandshake torrent_hash $ client' ^. client_peerId)
            messageOut >-> P.map renderProtocol
            >-> forever do await >>= TCP.send s
      do
        runEffectReporting do
          (peerNamed, producer) <- lift $ parseAll (peer_id initialPeer) (TCP.fromSocket s 17384)
          lift $ putMVar peerIdRef peerNamed
          producer
            >-> P.map (Peered peerNamed)
            >-> messageIn
    log' $ "Disconnected from " <> show initialPeer

-- | dynamic router for the output messages producer
outputRouter ::
  -- | outgoing messages
  Producer (Peered Protocol) (SafeT IO) () ->
  -- | consume new mailboxes
  Consumer (Peered (Output Protocol)) IO ()
outputRouter messageOut = do
  peers <- liftIO $ newTVarIO mempty
  liftIO $ forkSafeEffectLinked $
    messageOut
      >-> forever do
        Peered peer msg <- await
        liftIO $ do
          mp <- M.lookup peer <$> readTVarIO peers
          found <- forM mp $ \p -> atomically $ send p msg
          maybe 
            do warn'$  "Not found queue for peerId" <> show peer
            do const $ pure () 
            do found
  forever $ do
    Peered peer output <- await
    liftIO $ atomically $ modifyTVar peers $ at peer ?~ output

-- | connect to each consumed peer, TODO: have a (Consumer Peer IO ()) arg for dead peers
client ::
  -- | torrent definition
  TorrentInfo  ->
  -- | who we are
  Client ->
  -- | outgoing messages
  Producer (Peered Protocol) IO () ->
  -- | incoming messages
  Consumer (Peered Protocol) IO () ->
  -- | add peers on the run
  Consumer Peer IO ()
client torrent client' messagesOut messageIn =
  do
    (messageInWrite, messageInRead) <- liftIO $ spawn unbounded
    forkEffectLinked $ fromInput messageInRead >-> messageIn
    let processPeer newPeer =   do
          log' $ "Starting connection to " <> show newPeer
          -- peer box for outgoing messages
          (messageOutPeerWrite, messageOutPeerRead) <- liftIO $ spawn unbounded
          peerIdMVar <- liftIO  newEmptyMVar 
          forkEffectLinked $ keepAliveThread >-> toOutput messageOutPeerWrite
          liftIO $ fork $ reportException $ do
            connectToPeer
              newPeer
              torrent
              client'
              (fromInput messageOutPeerRead)
              (toOutput messageInWrite)
              peerIdMVar
            --FIXME NON MI RICORDO a che cazzo serve
          realPeerId <- liftIO $ readMVar peerIdMVar
          yield $ Peered realPeerId messageOutPeerWrite
    
        consumer = forever $ do
          newPeer <- await
          catchAll (processPeer newPeer) (liftIO . warn' . show) 
    consumer >-> outputRouter (hoist liftIO messagesOut)

-- | listen as a peer. TODO: have a (Consumer (Peer, Bool) IO ()) arg for peers come and go
server ::
  -- | torrent information
  TorrentInfo  ->
  -- | client information
  Client ->
  -- | outgoing messages
  Producer (Peered Protocol) (SafeT IO) () ->
  -- | incoming messages
  Consumer (Peered Protocol) (SafeT IO) () ->
  IO ()
server TorrentInfo  {..} client' messageOut messageIn = do
  (messageInWrite, messageInRead) <- liftIO $ spawn unbounded
  forkSafeEffectLinked $ fromInput messageInRead >-> messageIn
  reportException $ TCP.serve (TCP.Host $ toS $ _client_ip client') (show $ _client_port  client') $ \(s, _) -> do
    (peerNamed, producer) <- parseAll (Just $ client' ^. client_peerId) (TCP.fromSocket s 17384)
    runEffectReporting $
      do
        (messageOutPeerWrite, messageOutPeerRead) <- liftIO $ spawn unbounded
        log' $ "Connected to " <> show peerNamed
        forkEffectLinked $
          keepAliveThread
            >-> toOutput messageOutPeerWrite
        liftIO $ fork $ do
          race_
            do
              runEffectReporting $
                do
                  yield (renderHandshake torrent_hash (PeerId $ client' ^. client_ip))
                  fromInput messageOutPeerRead >-> P.map renderProtocol
                  >-> forever do await >>= TCP.send s
            do
              runEffectReporting do
                producer
                  >-> P.map (Peered peerNamed)
                  >-> toOutput messageInWrite
          log' $ "Disconnected from " <> show peerNamed
        yield $ Peered peerNamed messageOutPeerWrite
        >-> (hoist liftIO $ outputRouter messageOut)
