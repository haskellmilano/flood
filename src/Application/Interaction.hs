{-# LANGUAGE BlockArguments      #-}
{-# LANGUAGE ConstraintKinds     #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE NoImplicitPrelude   #-}
{-# LANGUAGE NumericUnderscores  #-}
{-# LANGUAGE ScopedTypeVariables #-}
-- {-# LANGUAGE TypeApplications #-}

module Application.Interaction where

-- import           Client
-- import           Control.Concurrent.STM
-- import           Control.Monad
-- import qualified Data.ByteString        as BS
-- import           Data.ByteString.Random
-- import qualified Data.Map.Strict               as M
-- import           Peer.Protocol
-- import Pipes.Prelude  (repeatM)
-- import           Protolude
-- import           Torrent.Info
-- import           Torrent.State
-- import           Tracker.Protocol
-- import Peer.Peer


-- --------------------------------------
-- -- control layer dumb unuseful example
-- --------------------------------------

-- data Control = Control
--     { send    :: Int -> Protocol -> IO ()
--     , receive :: Int -> IO [Either Text Protocol]
--     , peers   :: Set Int
--     , peerAt  :: Int -> IO Peer
--     }

-- -- connect to all peers and print messages
-- controlNetwork :: Info -> Client -> [Peer] -> IO Control
-- controlNetwork torrent client' ps = do
--     -- console reporting thread
--     messageOutChan <- newBroadcastTChanIO

--     -- boot all known peers
--     pcs <- forM (zip [1..] ps) $ \(n,p) -> do
--         peerT <- newTVarIO p
--         messageInChan <- newTChanIO
--         peerOutChan <- atomically $ dupTChan messageOutChan
--         peerMessageOutChan <- newTChanIO
--         _ <- forkIO $ forever $ atomically $ do
--             (pm,m) <- readTChan peerOutChan
--             when (pm == n) $
--                 writeTChan peerMessageOutChan m
--         _ <- forkIO $ startClient peerT torrent client'
--             do atomically . writeTChan messageInChan . (n,)
--             do repeatM $ atomically $ readTChan peerMessageOutChan
--         pure (n ,(peerT, messageInChan))

--     let pcsm = M.fromList pcs
--         waitPeer p = do
--             let  (_,chan) = pcsm M.! p
--             let w  = do
--                     mr <- atomically do tryReadTChan chan 
--                     case mr of
--                         Just (p',m) -> if p' == p
--                             then (m :) <$> w
--                             else w
--                         Nothing -> pure []
--             w
--         tell peer x = atomically $ writeTChan messageOutChan (peer, x)
--     pure $ Control tell waitPeer 
--         do M.keysSet pcsm
--         do \k -> readTVarIO $ fst $ pcsm M.! k

-- _connectPeers :: FilePath -> IO Control
-- _connectPeers path = do
--     f <- BS.readFile path
--     mi <- case parseMetaTorrent f of
--         Right meta_torrent -> pure meta_torrent
--         Left e -> panic $ show e
--     randomBytes <- random 13
--     let client' = Client ("-flood-" <> randomBytes) Nothing 6881
--     case _meta_torrent_info mi of
--         torrent@(SingleTorrent _name _pl _ps l _ms) -> do
--             let torrentState = TorrentState 0 0 l
--                 request = TrackerRequest client' torrentState (Just Started)
--             Swarm {..} <- getSwarm mi request
--             let peers = fromMaybe [] swarm_peers
--             controlNetwork torrent client' peers
--         _ -> panic "multi not supported"