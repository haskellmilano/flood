{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Strict #-}
{-# OPTIONS_GHC -Wno-partial-fields #-}

module Application.Main where

import Client
import Control.Lens hiding (each)
import Control.Logging
import qualified Data.ByteString as BS
import qualified Data.Map as M
import Data.Time
import Data.Maybe
import Lib
import Peer.Peer
import Peer.Protocol
import Pipes as P
import Pipes.Concurrent
import Pipes.Lift
import Pipes.Safe
import Protolude hiding (StateT, log)
import Request.Sequential
import Torrent.Have
import Torrent.Encoding 
import Torrent.State
import Torrent.Taint
import Torrent.Info 
import Torrent.Write
import Torrent.PeerLife
import System.IO (SeekMode(..), hSeek, hClose)
import qualified Pipes.Prelude as P


floodRequests ::
  (MonadSafe m, MonadState TorrentData m) =>
  Consumer (Peered Protocol) IO () ->
  m ()
floodRequests toPeers = do
  now <- liftIO getCurrentTime
  torrentData <- get
  runEffect $
    sequentialRequests now torrentData
      >-> forever do
        x <- await
        case x of
          msg@(Peered peer (Request taintIndex offset _)) -> do
            liftIO $ debug $ "sending request " <> show msg
            td_taints
              %= M.adjust
                (markRequested now (fromIntegral offset) peer)
                taintIndex
            yield msg
          _ -> panic "FIXME: partiality for free"
      >-> hoist liftIO toPeers

asideP :: Monad m => m () -> Pipe a a m ()
asideP f = forever do
  x <- await
  lift f
  yield x

freeRequests :: (MonadState TorrentData m, MonadIO m) => m ()
freeRequests = do
  now <- liftIO getCurrentTime
  use (td_taints . traverse . requested)
  td_taints . traverse %= cleanRequests (\u -> diffUTCTime now u > 10)

-- | A pipe to process incoming Protocol messages from peers
messagePipe ::
  -- | initial torrent state
  TorrentData ->
  TorrentInfo  ->
  Producer (Peered Protocol) IO () ->
  Consumer (Peered Protocol) IO () ->
  IO ()
messagePipe initialState torrent fromPeers toPeers = do
  fork $ void $ runSafeT $ runEffect
    $ evalStateP initialState
    $ let go n = do
            freeRequests
            floodRequests toPeers
            r <- next n
            case r of
              Right (a, n') -> yield a >> go n'
              Left r' -> warn' $ show r'
       in do
            go $ hoist (lift . lift . lift) fromPeers -- care ....
            >-> updateHave
            >-> updateChoke
            >-> writeToFile torrent "."



-- | wire all the pieces to download a torrent
downloadTorrentFile :: Client -> FilePath -> IO ()
downloadTorrentFile client' path = do
  torrentContent <- catchAll
    do BS.readFile path
    do errorL . show
  log' "TorrentInfo  content read"
  metaTorrent <- case parseMetaTorrent torrentContent of
    Right meta_torrent -> pure meta_torrent
    Left e -> errorL $ show e
  log' "TorrentInfo  info parsed"
  let torrent = fromTorrentEncoding (metaTorrent ^. meta_torrent_info)
  -- debug' $ "TorrentInfo  structure: " <> show torrent

  -- peer communication
  (messageInRead, messageOutWrite) <- handlePeers
    do TrackerPollInterval 1000_000_000
    client'
    metaTorrent
    torrent

  -- FIXME load from File system
  -- torrentData <- loadFromFiles "." client' torrent
  let torrentData = newTorrentData (client' ^. sub_piece_length) torrent
  messagePipe torrentData torrent messageInRead messageOutWrite
  -- log $ "terminated threads for torrent " <> torrent_name
  pure ()

serveTorrentInfo :: MonadSafe m => FilePath -> TorrentInfo  -> Pipe Protocol Protocol m ()
serveTorrentInfo filePath TorrentInfo{..} = do 
  handle' <-  liftIO $ openFile filePath ReadMode
  void $ register $ liftIO $ hClose handle'
  yield $ BitField $ Presence (fromIntegral  $ length torrent_taints) $ M.keysSet torrent_taints
  yield UnChoke
  forever $ do 
    p <- await
    case p of 
      Request index' begin length' -> do
        let offset = index' * fromIntegral torrent_taint_length + fromIntegral begin 
        liftIO $ hSeek handle' AbsoluteSeek  (fromIntegral offset)
        bits <- liftIO $ BS.hGet handle' (fromIntegral length')
        yield $ Piece index' begin bits

serveFile ::  TorrentEncoding ->  IO ()
serveFile encoding = do
    let info = fromTorrentEncoding encoding
    peerId <- liftIO generatePeerId
    let client' = Client  peerId "localhost" 999 444
    let filePath = fromJust $ head $ torrentFile_path $ torrent_files info M.! 0
    (prod, consumer) <- liftIO $ concurrentPipe $ serveTorrentInfo filePath info
    server info client' (prod >-> addPeerId peerId) (removePeerId >-> consumer) 


concurrentPipe :: MonadIO m => Pipe a a (SafeT IO) () -> m (Producer a (SafeT IO) (), Consumer a (SafeT IO) ())
concurrentPipe pipe = do  
    (outlet1, inlet1) <- liftIO $ spawn unbounded
    (outlet2, inlet2) <- liftIO $ spawn unbounded
    liftIO $ fork $ runSafeT $ runEffect $ fromInput inlet1 >-> pipe >-> toOutput outlet2
    pure (fromInput inlet2, toOutput outlet1)


removePeerId :: Functor m => Pipe (Peered a) a m ()
removePeerId = P.map peered_value

addPeerId:: Functor m => PeerId -> Pipe a (Peered a) m ()
addPeerId id = forever $ do
  a <- await
  yield $ Peered id a