{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings      #-}


module Bencoding
    ( roundTrip
    , stripLabel
    , module Codec.Encode
    , module Codec.Decode
    , module Codec.B
    , module Codec.Parse
    )
where

import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Codec.Decode
import           Codec.Encode
import           Codec.B
import           Codec.Parse
import Prelude

roundTrip :: (BDecode a, BEncode a) => a -> Either String a
roundTrip = bdecode . bencode

stripLabel :: Text -> Text -> Maybe Text
stripLabel p = clean . \case 
    (T.uncons -> Just ('_',xs)) -> xs
    xs -> xs
    where clean = fmap (T.replace "_" " ") . T.stripPrefix p 


