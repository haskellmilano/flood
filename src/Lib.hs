-- |
-- Module      : Lib
-- Description : Things we desperately need
-- Copyright   : (c) Haskell Milano, 2020
-- License     : BSD-3
-- Maintainer  : paolo.veronelli@email.com, @entangled90, @massimo
-- Stability   : experimental
-- Portability : POSIX
module Lib where

import qualified Data.Map as M
import Protolude

-- | build a 'Map' with an Enum key with a start point
mkIdxMap ::
  (Enum a, Eq a) =>
  -- | starting key 
  a ->
    -- | values
  [b] ->
    -- | resulting Map
  Map a b
mkIdxMap start = M.fromAscList . zip [start ..]

-- | fork a children, spawning thread control children's life
fork :: IO a -> IO ()
fork f = async f >>= link
