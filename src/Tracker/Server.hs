{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds, NoImplicitPrelude, OverloadedStrings #-}


module Tracker.Server where

import Protolude

import Tracker.Protocol
import qualified Data.ByteString.Char8 as BS
import Bencoding
import Network.Wai
import Network.HTTP.Types
import Network.Wai.Handler.Warp (run)
import Control.Logging
import Control.Concurrent.STM
import Peer.Peer
import Data.List (nub) 
import Control.Lens
import qualified Data.Map as M
import Data.Attoparsec.ByteString.Char8
import Network.Socket


type State = TVar Swarm 

addPeer :: Peer -> Swarm -> Swarm
addPeer p = over swarm_peers $ Just . nub . (p:) . fromMaybe []

getHost :: SockAddr -> ByteString
getHost (SockAddrInet _ (hostAddressToTuple -> (a,b,c,d))) =
    BS.intercalate "." $ show <$> [a, b, c, d]
getHost _ = panic "ipv6 not implemented"

getPeer :: Query -> Maybe (ByteString -> Peer)
getPeer q = do 
    id <- fmap PeerId <$>  (q' ^? ix "peer_id")
    -- ip <- join $ q' ^? ix "ip"
    port <- join (q' ^? ix "port") >>= \x-> parseOnly decimal x ^? _Right 
    pure $ \ip -> Peer ip port id
    where q' = M.fromList q


app :: TVar Swarm -> Application
app swarmT request respond = do
    swarm <- readTVarIO swarmT
    traverse_ 
        do atomically . modifyTVar swarmT . addPeer 
        do getPeer (queryString request) >>= \f -> pure $ f $ getHost $ remoteHost request 
    respond $ do
        responseLBS
            do status200
            do [("Content-Type", "application/octet-stream")]
            do  toS $ render  . bencode  $ swarm

emptySwarm :: Swarm
emptySwarm = Swarm 
    do Nothing
    do Nothing 
    do Nothing 
    do Nothing 

serverUp :: TChan () -> TChan () -> IO ()
serverUp stop restart = do
    swarmT <- newTVarIO emptySwarm
    server <- async $ withStderrLogging $  run 
        do 8081
        do app swarmT 
    atomically $ readTChan stop
    cancel server
    atomically $ writeTChan restart ()

