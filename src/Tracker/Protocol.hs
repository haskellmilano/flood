{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveDataTypeable    #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE MultiParamTypeClasses #-}
-- {-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeFamilies          #-}

{-# options_ghc -Wno-partial-fields #-}
module Tracker.Protocol where

import           Bencoding
import           Client
import           Control.Lens
import           Control.Monad
import           Data.Maybe
import           Generics.SOP.TH
import           Network.HTTP.Client
import           Network.HTTP.Client.TLS
import           Peer.Peer
import           Protolude
import           Torrent.Encoding 
import           Torrent.State
import qualified Data.ByteString as BS

data TrackerEvent = Started | Completed | Stopped deriving (Eq, Show)

showEvent :: IsString p => TrackerEvent -> p
showEvent Started   = "started"
showEvent Completed = "completed"
showEvent Stopped   = "stopped"

data TrackerRequest = TrackerRequest
    { tracker_request_client       :: Client
    , tracker_request_torrentState :: TorrentState
    , tracker_request_event        :: Maybe TrackerEvent
    }
    deriving (Show, Eq)



encodedParams :: TorrentEncoding -> TrackerRequest -> [(ByteString, Maybe ByteString)]
encodedParams torrent TrackerRequest {..} =
    [ ("info_hash" , Just $ infoHash torrent)
        , ("peer_id"   , Just $ getId $ _client_peerId tracker_request_client)
        , ("port"      , Just $ show $ _client_port tracker_request_client)
        -- , ("ip"      , showIP <$> _client_ip tracker_request_client)
        , ("uploaded"  , Just $ show $ _torrent_state_uploaded tracker_request_torrentState)
        , ("downloaded", Just $ show $ _torrent_state_downloaded tracker_request_torrentState)
        , ("left"      , Just $ show $ _torrent_state_left tracker_request_torrentState)
        , ("compact"   , Just "1")
        ]
        <> maybeToList ((\s -> ("event", Just $ showEvent s)) <$> tracker_request_event)

showIP :: (Word8, Word8, Word8, Word8) -> ByteString
showIP (a,b,c,d) = BS.intercalate "." $ map show [a,b,c,d]





fromCompact :: Swarm -> Swarm
fromCompact SwarmCompact {..} = Swarm
    do mkPeer4 <$> _swarm_peers_compact
    do mkPeer6 <$> swarm_peers6_compact
    do swarm_interval
    do swarm_min_interval

fromCompact x = x

data Swarm =                    Swarm
     {     _swarm_peers          :: Maybe [Peer]
     ,     swarm_peers6         :: Maybe [Peer]
     ,     swarm_interval       :: Maybe Integer
     ,     swarm_min_interval   :: Maybe Integer
     }     |                    SwarmCompact
     {     _swarm_peers_compact  :: Maybe ByteString
     ,     swarm_peers6_compact :: Maybe ByteString
     ,     swarm_interval       :: Maybe Integer
     ,     swarm_min_interval   :: Maybe Integer
     }
    deriving (Show,Eq)

swarmLabel :: Text -> Maybe Text
swarmLabel x = do
    l <- stripLabel "swarm_" x
    pure $ case l of
        "peers compact" -> "peers"
        "peers6 compact" -> "peers6"
        x'             -> x'

deriveGeneric ''Swarm
makeLenses ''Swarm
instance BDecode Swarm where
    bdecode = gbdecode  swarmLabel
instance BEncode Swarm where
    bencode = gbencode swarmLabel

getSwarm :: MetaTorrent -> TrackerRequest -> IO Swarm
getSwarm torrent req = do
    manager <- newTlsManager
    request <- parseRequest $ toS $ meta_torrent_announce torrent
    let addQueryString = setQueryString $ encodedParams (_meta_torrent_info torrent) req
    r <- toS . responseBody <$> httpLbs (addQueryString request) manager
    case over _Left toS (parse r) >>= bdecode of
        Left  e -> fail $ show (e,r)
        Right x -> pure $ fromCompact x
