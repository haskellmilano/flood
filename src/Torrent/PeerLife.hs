{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Strict #-}
{-# OPTIONS_GHC -Wno-partial-fields #-}

-- |
-- Module      : Torrent.PeerLife
-- Description : Peer list handling
-- Copyright   : (c) Haskell-Milano
-- License     : BSD
-- Maintainer  : paolo.veronelli@gmail.com, @entangled90, @massimo
-- Stability   : experimental
-- Portability : POSIX
module Torrent.PeerLife where

import Client
import Control.Lens hiding (each)
import Control.Logging
import qualified Data.Set as S
import Lib
import Peer.Peer
import Peer.Protocol
import Pipes as P
import Pipes.Concurrent
import qualified Pipes.Prelude as P
import Protolude hiding (StateT, log)
import Torrent.Encoding
import Torrent.State
import Torrent.Info 
import Tracker.Protocol

-- | send Interested to all peers
interestedThread :: [PeerId] -> Consumer (Peered Protocol) IO () -> IO ()
interestedThread peers toPeers = forever do
  runEffect $
    each peers
      >-> P.map
        do (`Peered` Interested)
      >-> toPeers
  threadDelay 10_000_000

trackerRequest :: Client -> TorrentInfo  -> TrackerRequest
trackerRequest client' torrent = TrackerRequest
  do client'
  do TorrentState 0 0 (sum $ map (fromIntegral . view _2) $ torrent_taints torrent)
  do Just Started

-- interval in microseconds 
newtype TrackerPollInterval = TrackerPollInterval {trackerPollInterval :: Int}

handlePeers ::
  TrackerPollInterval ->
  Client ->
  MetaTorrent ->
  TorrentInfo  ->
  IO
    ( Producer (Peered Protocol) IO (), -- messages from the peers
      Consumer (Peered Protocol) IO () -- messages to the peers
    )
handlePeers TrackerPollInterval {..} client' metaTorrent torrent = do
  (messageOutWrite, messageOutRead) <- liftIO $ spawn unbounded
  (messageInWrite, messageInRead) <- liftIO $ spawn unbounded
  fork $ flip evalStateT mempty $ forever $ do
    Swarm {..} <- liftIO $ getSwarm metaTorrent $ trackerRequest client' torrent
    oldPeers <- get
    let newPeers = S.difference
          do S.fromList $ fromMaybe [] _swarm_peers
          do oldPeers
    put $ oldPeers <> newPeers
    log' $ "Got " <> show (length newPeers) <> " peers"
    let peerIds = mapMaybe peer_id (toList newPeers)
    liftIO $ runEffectReporting $
      P.each newPeers
        >-> client
          torrent
          client'
          (fromInput messageOutRead)
          (toOutput messageInWrite)
    liftIO $ log' $ "Fork terminated for all new peers (" <> show (length newPeers) <> ")"
    liftIO $ fork $ interestedThread peerIds $ toOutput messageOutWrite
    liftIO $ threadDelay trackerPollInterval
  pure (fromInput messageInRead, toOutput messageOutWrite)
