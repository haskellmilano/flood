{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Strict #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}

-- |
-- Module      : Torrent.State
-- Description : State of a downloading torrent
-- Copyright   : (c) Haskell Milano, 2020
-- License     : BSD-3
-- Maintainer  : paolo.veronelli@email.com, @entangled90, @massimo
-- Stability   : experimental
-- Portability : POSIX
module Torrent.State where

import Control.Concurrent.STM
import Control.Lens
import Control.Logging (log')
import qualified Data.Map as M
import Peer.Peer
import Peer.Protocol
import Pipes (Pipe, await, yield)
import Protolude hiding (to)
import Torrent.Taint (PieceLength, Taint, TaintIndex, mkTaint)
import Torrent.Info 

data ChokeState = Choked | Unchoked deriving (Eq, Show)

makePrisms ''ChokeState

-- | full state of a torrent
data TorrentData = TorrentData
  { -- | the state of the pieces we have for each file
    _td_taints :: Map TaintIndex Taint,
    -- | pieces peers have
    _td_peers_have :: Map PeerId (Set TaintIndex),
    -- | how peers see us
    _td_chockedBy :: Map PeerId ChokeState
  }

makeLenses ''TorrentData

-- | boot a torrent state given a 'Torrent' context and the granularity of a Taint
newTorrentData :: PieceLength -> TorrentInfo  -> TorrentData
newTorrentData pieceLen targets = TorrentData (mkTaint pieceLen . view _2 <$> torrent_taints targets) mempty mempty

-- | no idea
data TorrentState = TorrentState
  { _torrent_state_uploaded :: !Integer,
    _torrent_state_downloaded :: !Integer,
    _torrent_state_left :: !Integer
  }
  deriving (Show, Eq)

makeLenses ''TorrentState

-- | communication to and from peers for a torrent
data TorrentShare a = TorrentShare
  { -- | messages out to peers
    ts_messageOut :: Map Peer (TChan a),
    -- | messages in from peers
    ts_messageIn :: Map Peer (TChan a)
  }

-- | create an empty shared memory for a torrent
newTorrentShare :: [Peer] -> STM (TorrentShare a)
newTorrentShare ps = TorrentShare
  <$> fmap
    M.fromList
    do forM ps $ \p -> (p,) <$> newTChan
  <*> fmap
    M.fromList
    do forM ps $ \p -> (p,) <$> newTChan

updateChoke ::
  (MonadState TorrentData m, MonadIO m) =>
  Pipe (Peered Protocol) (Peered Protocol) m ()
updateChoke = forever $ do
  value@(Peered p v) <- await
  let f x = do
        td_chockedBy %= M.insert p x
        log' $ "Peer " <> show p <> " says " <> show x
  case v of
    Choke -> f Choked
    UnChoke -> f Unchoked
    _ -> yield value
