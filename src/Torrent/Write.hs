{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StarIsType #-}
{-# LANGUAGE TypeFamilies #-}

module Torrent.Write where

-- import           Control.Concurrent.STM
import Control.Lens
import Control.Logging
import qualified Crypto.Hash.SHA1 as SHA1
import qualified Data.ByteString as BS
import qualified Data.Map.Strict as M
import Peer.Peer
import Peer.Protocol
import Pipes
import Pipes.Safe
import Protolude hiding (bracket, hash, log, to)
import System.FilePath
import System.IO hiding (print)
import Torrent.Encoding
import Torrent.State
import Torrent.Taint
import Torrent.Info 

-- |
-- Module      : Torrent.Write
-- Description : 
-- Copyright   : (c) Haskell Milano, 2020
-- License     : BSD-3
-- Maintainer  : paolo.veronelli@email.com, @entangled90, @massimo
-- Stability   : experimental
-- Portability : POSIX
--
-- 
-- TODO: atomically work on taintedsT

initFile :: MonadIO m => FilePath -> TorrentFile -> m Handle
initFile baseFolder tf = liftIO $ do
  let path = baseFolder </> torrent_filePath tf
  -- zeroFile (tf ^. torrent_file_length) path
  log' $ "Openfile at path" <> show path
  openFile path ReadWriteMode -- open the right file

-- | consume Piece peer-protocol events
writeToFile ::
  (MonadState TorrentData m, MonadSafe m) =>
  -- | dowloading torrent
  -- -> Handle -- ^ file opened
  TorrentInfo  ->
  -- | base directory
  FilePath ->
  Consumer (Peered Protocol) m ()
writeToFile t@TorrentInfo  {..} fp = do
  let baseFolder = fp </> base_folder t
  createBaseFolder "." t
  handles <- traverse (initFile baseFolder) torrent_files
  void $ register $ liftIO $ traverse_ hClose handles
  forever $ do
    Peered peer x <- await
    -- debug' $ show m
    case x of
      Piece taintIdx offset bytes -> do
        -- release peer from uploaders
        -- Map TaintIndex Taint
        tainteds <- preuse td_taints
        let subPieceInfo = do
              fileTaint <- tainteds
              taint <- M.lookup taintIdx fileTaint
              peered <- request (fi offset) taint
              (_hash, _length, TaintCoordinate fid taintOffset) <- torrent_taints ^? ix taintIdx
              handle' <- handles ^? ix fid
              pure (peered, handle', taintOffset)
        case subPieceInfo of
          Nothing ->
            warn' $
              "Dropping taintIdx, request not found for taintIdx="
                <> show taintIdx
                <> "and offset="
                <> show offset
          Just (Peered peerOfRequest (end, _requestTime), torrentHandle, taintOffset) ->
            let intervalLen = fi $ end - fi offset
             in if (peer, BS.length bytes) == (peerOfRequest, intervalLen)
                  then do
                    liftIO $ do
                      hSeek torrentHandle AbsoluteSeek $ fi taintOffset + fi offset
                      BS.hPut torrentHandle bytes
                      hFlush torrentHandle -- necessary for tests , TODO remove
                      -- log $  "wrote offset " <> show end <> "for taintIdx " <> show taintIdx
                    taintIsCompleted <- taintPieces taintIdx (fromIntegral offset)
                    when taintIsCompleted (checkValidHash t taintIdx torrentHandle)
                  else
                    warn' $
                      "Wrong peer or length for: (taintIdx,offset)" <> show (taintIdx, offset)
                        <> "sizes are: (received, expected) ->"
                        <> show (BS.length bytes, intervalLen)
                        <> show (peer, peerOfRequest)
      _ -> pure ()

fi :: Integral a => Integral b => a -> b
fi = fromIntegral

checkValidHash ::
  (MonadIO m, MonadState TorrentData m) =>
  TorrentInfo  ->
  TaintIndex ->
  Handle ->
  m ()
checkValidHash torrent taintIdx fileHandle = do
  let maybeHash = torrent ^? to torrent_taints . ix taintIdx
  isHashValid <- case maybeHash of
    Just (validHash, taintLength, TaintCoordinate _filePath (FileOffset taintOffset)) ->
      liftIO $ do
        hSeek fileHandle AbsoluteSeek $ fromIntegral taintOffset
        hashedContent <- TaintHash . SHA1.hash <$> BS.hGet fileHandle (fromIntegral taintLength)
        let isHashValid = hashedContent == validHash
        unless isHashValid $ errorL' $
          "Wrong hash for taintIdx: expected=" <> show validHash
            <> ", found="
            <> show hashedContent
        pure isHashValid
    Nothing ->
      errorL' "Could not find expected hash for taintIdx"
  if isHashValid
    then do
      liftIO $ log' $ "Piece " <> show taintIdx <> " is complete."
      td_taints %= M.delete taintIdx
    else do
      warn' $ "Taint" <> show taintIdx <> "has wrong hash!"
      td_taints %= M.adjust setAllMissing taintIdx

-- | mark subpieces as downloaded
taintPieces ::
  (MonadIO m, MonadState TorrentData m) =>
  TaintIndex ->
  PieceOffset ->
  m Bool
taintPieces taintIdx offset = do
  td_taints %= M.adjust (markDownloaded offset) taintIdx
  taint <- preuse $ td_taints . ix taintIdx
  case taint of
    Just t -> do
      liftIO $ debug' $ "Received subpiece of taintIdx " <> show taintIdx
      pure $ isComplete t
    Nothing -> errorL' $ "Cannot find taint for idx" <> show taintIdx

-- | fill up a file with 0s
-- zeroFile
--     :: Integer  -- ^ file size
--     -> FilePath  -- ^ file name
--     -> IO ()
-- zeroFile l f = do
--     b <- doesPathExist f
--     unless
--         b
--         do
--             withFile f WriteMode $ \hOut ->
--                 runEffect
--                     $   (PP.replicateM n z <> yield (BS.replicate r 0))
--                     >-> P.toHandle hOut
--   where
--     (fromIntegral -> n, fromIntegral -> r) = divMod l 1024
--     z = pure $ BS.replicate 1024 0
