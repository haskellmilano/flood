{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wno-partial-fields #-}

-- |
-- Module      : Torrent.Info
-- Description : From a file .torrent to a 'Torrent'
-- Copyright   : (c) Haskell Milano, 2020
-- License     : BSD-3
-- Maintainer  : paolo.veronelli@email.com, @entangled90, @massimo
-- Stability   : experimental
-- Portability : POSIX
--
-- Parses torrent files and produce the static content needed to process the torrent
module Torrent.Encoding where

import Bencoding
import Control.Lens
import Crypto.Hash.SHA1 as SHA1
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC
import Data.List ()
import qualified Data.Map.Strict as M
import Generics.SOP.TH
import Lib
import Protolude
import System.Directory (createDirectoryIfMissing)
import System.FilePath ((</>))
import Torrent.Info
import Torrent.Taint
import Prelude (String)

-- | local parse monad
type ParseInfo = Either String

-- | the way multifiles torrentdescribe files
data TorrentFileEncoding = TorrentFileEncoding
  { torrent_file_enc_length :: Integer,
    torrent_file_enc_path :: [Text],
    torrent_file_enc_md5sum :: Maybe ByteString
  }
  deriving (Show, Eq)

deriveGeneric ''TorrentFileEncoding

instance BDecode TorrentFileEncoding where
  bdecode = gbdecode (stripLabel "torrent_file_enc_")

instance BEncode TorrentFileEncoding where
  bencode = gbencode (stripLabel "torrent_file_enc_")

-- | TorrentEncoding once hash splitting happened, TODO: remove splitting at this level

-- | Alternative encoding for single and multifile torrent
data TorrentEncoding
  = SingleTorrent
      { -- |  name of the file
        torrent_enc_name :: Text,
        -- |  standard piece length
        torrent_enc_piece_length :: Integer,
        -- |  information about pieces hash
        _torrent_enc_pieces :: ByteString,
        -- |  total size of the file
        torrent_enc_length :: Integer,
        -- | checksum of     something
        torrent_enc_md5sum :: Maybe ByteString
      }
  | MultiTorrent
      { torrent_enc_name :: Text,
        torrent_enc_piece_length :: Integer,
        _torrent_enc_pieces :: ByteString,
        torrent_enc_files :: [TorrentFileEncoding]
      }
  deriving (Show, Eq)

deriveGeneric ''TorrentEncoding
makeLenses ''TorrentEncoding
makePrisms ''TorrentEncoding

instance BDecode TorrentEncoding where
  bdecode = gbdecode ((<|>) <$> stripLabel "_torrent_enc_" <*> stripLabel "torrent_enc_")

instance BEncode TorrentEncoding where
  bencode = gbencode ((<|>) <$> stripLabel "_torrent_enc_" <*> stripLabel "torrent_enc_")

-- | Meta torrent encoding
data MetaTorrent = MetaTorrent
  { meta_torrent_announce :: ByteString,
    _meta_torrent_info :: TorrentEncoding,
    meta_torrent_announce_list :: Maybe [[ByteString]],
    meta_torrent_comment :: Maybe Text,
    meta_torrent_created_by :: Maybe Text,
    meta_torrent_creation_date :: Maybe Integer,
    meta_torrent_url_list :: Maybe [Text]
  }
  deriving (Show, Eq)

deriveGeneric ''MetaTorrent
makeLenses ''MetaTorrent

patchMetaFields :: Text -> Text
patchMetaFields "url list" = "url-list"
patchMetaFields "announce list" = "announce-list"
patchMetaFields other = other

mapFields :: Text -> Maybe Text
mapFields = fmap patchMetaFields
  . do
    (<|>)
      <$> stripLabel "_meta_torrent_"
      <*> stripLabel "meta_torrent_"

instance BDecode MetaTorrent where
  bdecode = gbdecode mapFields

instance BEncode MetaTorrent where
  bencode = gbencode mapFields

-- | full parser
parseMetaTorrent :: ByteString -> ParseInfo MetaTorrent
parseMetaTorrent bs = do
  b <- over _Left toS $ parse bs
  bdecode b

-- | full renderer
renderMetaTorrent :: MetaTorrent -> ByteString
renderMetaTorrent = render . bencode

-- | split hash
splitHash :: ByteString -> [ByteString]
splitHash = unfoldr chunkBy
  where
    chunkBy "" = Nothing
    chunkBy b = pure $ BC.splitAt 20 b

-- | compute the hash of of the TorrentEncoding
infoHash :: TorrentEncoding -> ByteString
infoHash = SHA1.hash . render . bencode

-- | create the folder where to store the torrent result
createBaseFolder ::
  MonadIO m =>
  -- | base directory
  FilePath ->
  -- | TorrentInfo  info
  TorrentInfo ->
  m ()
createBaseFolder fp torrent = liftIO $ createDirectoryIfMissing False (fp </> base_folder torrent)

-- not sure
base_folder :: TorrentInfo -> FilePath
base_folder TorrentInfo {..} = if length torrent_files > 1 then toS torrent_name else "."

-- | path of a file from its segments
torrent_filePath :: TorrentFile -> FilePath
torrent_filePath = foldr (</>) "" . fmap toS . torrentFile_path

-- | compute taint contexts of a torrent
mkTaintContexts ::
  -- | given standard taint length
  TaintLength ->
  -- | structure of the content of a torrent
  Map FileIndex TorrentFile ->
  -- | taint contexts indexed by new taint ids
  Map TaintIndex (TaintLength, TaintCoordinate)
mkTaintContexts (TaintLength l) fs = fold $ unfoldr f (0, M.assocs fs)
  where
    f (_, []) = Nothing
    f (n, (fid, TorrentFile (FileLength fl) _ _) : fs') =
      let (dn, rest) = divMod fl l
          p = [ M.singleton
                  (TaintIndex $ n + t)
                  (TaintLength l, FileOffset $ t * l)
                | t <- [0 .. dn - 1]
              ]
            <> case rest of
              0 -> mempty
              _ -> pure $ M.singleton (TaintIndex $ n + dn) (TaintLength rest, FileOffset $ dn * l)
       in Just
            ( fmap (TaintCoordinate fid) <$> fold p,
              (n + dn + signum rest, fs')
            )

-- | most general way to build a 'Torrent' value
mkTorrent ::
  -- | name
  Text ->
  -- | hashes
  [TaintHash] ->
  -- | hash
  ByteString ->
  -- | standard taint length
  TaintLength ->
  -- | files
  Map FileIndex TorrentFile ->
  TorrentInfo
mkTorrent torrent_name hashes torrent_hash torrent_taint_length torrent_files = TorrentInfo {..}
  where
    taintContexts = mkTaintContexts torrent_taint_length torrent_files
    torrent_taints = fold do
      (h, (i, (l, c))) <- zip hashes $ M.assocs taintContexts
      pure $ M.singleton i (h, l, c)

{-# ANN module ("HLint: ignore Reduce duplication" :: Text) #-}

-- | TorrentInfo  value ouf of a decoded .torrent file
fromTorrentEncoding :: TorrentEncoding -> TorrentInfo
fromTorrentEncoding enc =
  mkTorrent
    do torrent_enc_name enc
    do TaintHash <$> splitHash (_torrent_enc_pieces enc)
    do infoHash enc
    do TaintLength $ fromIntegral $ torrent_enc_piece_length enc
    torrentFiles
  where
    torrentFiles = case enc of
      SingleTorrent {..} ->
        mkIdxMap 0 $ pure $
          TorrentFile
            (fromIntegral torrent_enc_length)
            [toS torrent_enc_name]
            torrent_enc_md5sum
      MultiTorrent {..} -> mkIdxMap 0 $ mkSingleFile <$> torrent_enc_files
        where
          mkSingleFile TorrentFileEncoding {..} =
            TorrentFile
              (fromIntegral torrent_file_enc_length)
              (toS <$> torrent_file_enc_path)
              torrent_file_enc_md5sum

mkSingleTorrent ::
  -- | taint length
  TaintLength ->
  -- | torrent content
  ByteString ->
  -- | torrent name
  Text ->
  -- |
  TorrentEncoding
mkSingleTorrent taint_len content torrent_enc_name = SingleTorrent {..}
  where
    torrent_enc_piece_length = fromIntegral $ fromTaintLength taint_len
    _torrent_enc_pieces = B.concat $ unfoldr f content
      where
        f :: ByteString -> Maybe (ByteString, ByteString)
        f bs = case B.splitAt (fromIntegral torrent_enc_piece_length) bs of
          ("", _) -> Nothing
          (piece, rest) -> Just (SHA1.hash piece, rest)
    torrent_enc_length = fromIntegral $ B.length content
    torrent_enc_md5sum = Nothing

