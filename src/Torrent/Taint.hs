{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Strict #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wno-orphans #-}

-- |
-- Module      : Torrent.Taint
-- Description : Basic structure to remember micro pieces 'Piece' state
-- Copyright   : (c) Haskell Milano, 2020
-- License     : BSD-3
-- Maintainer  : paolo.veronelli@email.com, @entangled90, @massimo
-- Stability   : experimental
-- Portability : POSIX
module Torrent.Taint where

import Control.Lens
import Control.Monad.Trans.State.Strict
import qualified Data.Map.Strict as Map
import Data.Time
import Peer.Peer
import Protolude hiding (State, StateT, get, put, state)

-- | position relative/internal to a macropiece
newtype PieceOffset = PieceOffset Int64
  deriving (Eq, Show, Ord, Integral, Enum, Real, Num)

newtype PieceLength = PieceLength Int64
  deriving (Eq, Show, Ord, Integral, Enum, Real, Num)

newtype TaintIndex = TaintIndex Int64
  deriving (Eq, Show, Ord, Integral, Enum, Real, Num)

newtype TaintLength = TaintLength {fromTaintLength :: Int64}
  deriving (Eq, Show, Ord, Integral, Enum, Real, Num)

newtype TaintHash = TaintHash {unTaintHash :: ByteString}
  deriving (Eq, Show, Ord, IsString)


-- | state of a downloading torrent macropiece
data Taint = Taint
  { -- | missing pieces
    _missing :: Map PieceOffset PieceOffset,
    -- | requested pieces, carry info of the requested peer and time of request
    _requested :: Map PieceOffset (Peered (PieceOffset, UTCTime)),
    -- | downloaded pieces, carry info of the uploader
    _downloaded :: Map PieceOffset (Peered PieceOffset)
  }
  deriving (Eq, Show)

makeLenses ''Taint

mkIntervals ::
  Integral a =>
  -- | full length
  a ->
  -- | cut length
  a ->
  -- | (idx, length) of the piece
  [(a, a)]
mkIntervals len subLen =
  [(i, subLen) | i <- [0 .. n - 1]] <> [(n, r) | r /= 0]
  where
    (n, r) = divMod len subLen

-- | boot as missings starting from divisors
mkTaint :: PieceLength -> TaintLength -> Taint
mkTaint (PieceLength mpl) (TaintLength tl) =
  Taint
    (Map.fromList [(PieceOffset (x * mpl), PieceOffset $ (x * mpl) + d) | (x, d) <- as])
    mempty
    mempty
  where
    as = mkIntervals tl mpl

-- | promote a missing to a requested for a given peer
markFirstRequested ::
  Monad m =>
  -- | request time
  UTCTime ->
  -- | uploader
  PeerId ->
  StateT Taint m (Maybe (PieceOffset, PieceOffset))
markFirstRequested now peer = do
  t <- get
  case Map.lookupMin (t ^. missing) of
    Just (k, v) -> markRequestedS now k peer $> Just (k, v)
    Nothing -> pure Nothing

-- | promote a specific missing to a requested
markRequested :: UTCTime -> PieceOffset -> PeerId -> Taint -> Taint
markRequested u id peer t =
  case t ^. missing ^? ix id of
    Just v ->
      t & over missing (Map.delete id)
        & over requested (Map.insert id $ Peered peer (v, u))
    Nothing -> t

-- | promote a specific missing to a requested
markRequestedS :: (Monad m) => UTCTime -> PieceOffset -> PeerId -> StateT Taint m ()
markRequestedS u id = liftT $ markRequested u id

-- | promote a requested to a downloaded
markDownloaded :: PieceOffset -> Taint -> Taint
markDownloaded id t = case t ^. requested ^? ix id of
  Just p ->
    t & requested %~ Map.delete id
      & downloaded %~ Map.insert id (fst <$> p)
  Nothing -> t

-- | promote a requested to a downloaded
markDownloadedS :: PieceOffset -> State Taint ()
markDownloadedS = liftT markDownloaded

-- | test purpose, full piece marker
markAllDownloaded :: State Taint ()
markAllDownloaded = do
  initial <- get
  traverse_ markDownloadedS (Map.keys $ initial ^. requested)

-- | test purpose full piece marker
markAllRequested :: UTCTime -> PeerId -> State Taint [(PieceOffset, PieceOffset)]
markAllRequested now peer = do
  initial <- get
  traverse_ (\x -> markRequestedS now x peer) (Map.keys $ initial ^. missing)
  pure $ Map.assocs $ initial ^. missing

-- | check a taint is fully downloaded, TODO: cache
isComplete :: Taint -> Bool
isComplete t = null (t ^. missing) && null (t ^. requested)

-- | query a requested
request :: PieceOffset -> Taint -> Maybe (Peered (PieceOffset, UTCTime))
request k t = t ^. requested ^? ix k

-- | test a requested
isRequested :: PieceOffset -> Taint -> Bool
isRequested k t = has _Just $ request k t

-- | test a missing
isMissing :: PieceOffset -> Taint -> Bool
isMissing k t = has _Just $ t ^. missing ^? ix k

-- | check a Taint has some missing
hasMissing :: Taint -> Bool
hasMissing t = not $ null $ t ^. missing

liftT :: Monad m => (a -> b -> b) -> a -> StateT b m ()
liftT f a = state $ \s -> pure $ f a s

-- | downgrade timed-out requests to missings
cleanRequests :: (UTCTime -> Bool) -> Taint -> Taint
cleanRequests f (Taint ms rqs dls) = Taint ms' rqs'' dls
  where
    (rqs', rqs'') = Map.partition (f . snd . peered_value) rqs
    ms' = ms <> (fst . peered_value <$> rqs')

-- must be used only when is completed
setAllMissing :: Taint -> Taint
setAllMissing t@Taint {..} =
  if isComplete t
    then Taint (fmap peered_value _downloaded) mempty mempty
    else panic $ "invalid taint, cannot be set to all missing " <> show t
