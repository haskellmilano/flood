{-# LANGUAGE BlockArguments   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Torrent.Have where

import           Control.Lens
import           Control.Logging
import qualified Data.Map.Strict as M
import qualified Data.Set        as S
import           Peer.Peer
import           Peer.Protocol
import           Pipes
import           Protolude       hiding (bracket, log)
import           Torrent.State

reverseMap :: Ord a => Ord k => Map k (Set a) -> Map a (Set k)
reverseMap m = M.unionsWith (<>) $ do
    (k  ,s ) <- M.assocs m
    a  <- S.toList s
    pure $ M.singleton a $ S.singleton k

updateHave :: (MonadIO m , MonadState TorrentData m) =>  Pipe (Peered Protocol) (Peered Protocol) m ()
updateHave  = forever $ do
    Peered p v <- await
    let change = (td_peers_have %=)
    case v of
        BitField (Presence _ s) -> do
            liftIO $ log $ "Received bitfield from peer " <> show p
            change $ M.insert p s
        Have i -> do
            liftIO $ log $ "Received have from peer " <> show p <> " for piece " <>  show i  
            change $ M.alter 
                do Just . maybe (S.singleton i) (S.insert i)
                do p
        e -> yield $ Peered p e
        