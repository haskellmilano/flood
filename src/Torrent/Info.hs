{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

-- |
-- Module      : Torrent.Info
-- Description : Immutable context of a torrent
-- Copyright   : (c) Haskell Milano, 2020
-- License     : BSD-3
-- Maintainer  : paolo.veronelli@email.com, @entangled90, @massimo
-- Stability   : experimental
-- Portability : POSIX
module Torrent.Info where

import Control.Lens
import Protolude hiding (to)
import Torrent.Taint

-- | we need to index files to support many to one relation with taints
newtype FileIndex = FileIndex Int deriving (Show, Eq, Enum, Num, Ord)

-- |
newtype FileLength = FileLength Int64 deriving (Show, Eq, Integral, Enum, Real, Num, Ord)

-- | A position inside a file
newtype FileOffset = FileOffset Int64 deriving (Show, Eq, Integral, Enum, Num, Real, Ord)

-- | Coordinate of a Taint in the torrent content files
data TaintCoordinate = TaintCoordinate FileIndex FileOffset deriving (Show, Eq)

-- | Static info data about a file in a torrent
data TorrentFile = TorrentFile
  { torrentFile_length :: FileLength,
    torrentFile_path :: [FilePath],
    torrentFile_md5sum :: Maybe ByteString
  }
  deriving (Eq, Show)

makeLenses 'TorrentFile

-- | full context of a torrent for torrenting it
data TorrentInfo = TorrentInfo
  { -- |  name of the file
    torrent_name :: Text,
    -- |  standard piece length
    torrent_taint_length :: TaintLength,
    -- | hash used to identify this torrent (hash of the pieces hash)
    torrent_hash :: ByteString,
    -- |  information about pieces hash
    torrent_taints :: Map TaintIndex (TaintHash, TaintLength, TaintCoordinate),
    torrent_files :: Map FileIndex TorrentFile
  }
  deriving (Show, Eq)

makeLenses 'TorrentInfo

  
{- mkInfo :: p -> Text -> TorrentInfo
mkInfo bytes' torrent_name =  TorrentInfo {..} where
  torrent_taint_length = 0
  torrent_hash = ""
  torrent_taints = mempty
  torrent_files = mempty -}
