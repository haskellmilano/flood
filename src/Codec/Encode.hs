
{-# LANGUAGE BlockArguments       #-}
{-# LANGUAGE ConstraintKinds      #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TypeApplications     #-}
{-# LANGUAGE UndecidableInstances #-}


module Codec.Encode where
import Prelude
import           Codec.B
import           Data.ByteString    (ByteString)
import qualified Data.Map.Strict           as M
import           Data.Maybe         (fromMaybe)
import           Data.Text          (Text)
import qualified Data.Text          as T
import           Data.Text.Encoding (encodeUtf8)
import           Generics.SOP       hiding (I, S)
import qualified Generics.SOP       as G




class BEncode a where
    bencode :: a -> B

instance BEncode ByteString where
    bencode = S
instance BEncode Text where
    bencode = S . encodeUtf8
instance BEncode Integer where
    bencode = I
instance BEncode a => BEncode [a] where
    bencode a = L $ bencode <$> a

class BEncodeMaybe a where
    bencodeMaybe :: a -> Maybe B

instance BEncode a => BEncodeMaybe (Maybe a) where
    bencodeMaybe a = (bencode @ a) <$> a 

instance {-# overlappable #-} BEncode a => BEncodeMaybe a where
    bencodeMaybe a = Just $ bencode a 

type ElemC = All  BEncodeMaybe

encodeConstructor :: forall a . ElemC a => (Text -> Maybe Text) 
    -> ConstructorInfo a -> NP G.I a -> K B a
encodeConstructor p (Record _ ns) = K . D . M.unions . hcollapse . hcliftA2
    do Proxy :: Proxy BEncodeMaybe
    do \(FieldInfo name) (G.I (x :: q)) ->  fromMaybe mempty $ do
                stripped <- p  $ T.pack name
                mv <- bencodeMaybe x
                pure $ K $ M.singleton stripped mv
    do ns

encodeConstructor _ _ =  error "constructor not supported because of maybe supported"

type ElemC2 a = All2 BEncodeMaybe (Code a)

gbencode 
    :: forall a 
    . ( HasDatatypeInfo a, ElemC2 a) 
    => (Text -> Maybe Text) -> a -> B
gbencode mp a = hcollapse $ hcliftA2 
        do Proxy :: Proxy ElemC
        do encodeConstructor mp 
        do constructorInfo $ datatypeInfo (Proxy :: Proxy a)
        do unSOP $ from a


instance {-# overlappable #-} (HasDatatypeInfo a, ElemC2 a)
   => BEncode a where
    bencode = gbencode pure




