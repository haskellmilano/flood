{-# language NoImplicitPrelude, TemplateHaskell #-}

module Codec.B where

import Control.Lens
import Protolude

-- | BEncoding AST
data B = S ByteString | I Integer | L [B] | D (Map Text B)  deriving (Eq, Show)

makePrisms ''B