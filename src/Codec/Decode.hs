{-# LANGUAGE BlockArguments       #-}
{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE KindSignatures       #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE StarIsType           #-}
{-# LANGUAGE UndecidableInstances #-}


module Codec.Decode where
import Prelude
import           Codec.B
import           Control.Monad
import           Data.ByteString    (ByteString)
import           Data.Kind          (Type)
import qualified Data.Map.Strict           as M
import           Data.Text          (Text)
import qualified Data.Text          as T
import           Data.Text.Encoding (decodeUtf8')
import           Generics.SOP       hiding (I, S)
import qualified Generics.SOP       as G


class  BDecode a where
    bdecode ::  B -> Either String a

instance BDecode ByteString where
    bdecode (S x) = pure x
    bdecode _ = Left "bytestring not in S"
instance BDecode Text where
    bdecode (S x) = case decodeUtf8' x of
        Right y -> pure y
        Left e -> Left $ "couldn't decode UTF8" <> show e
    bdecode _ = Left "bytestring not in S"
instance BDecode Integer where
    bdecode (I x) = pure x
    bdecode _ = Left "integer not in I"

instance BDecode a => BDecode [a] where
    bdecode (L l) = traverse bdecode l
    bdecode _ = Left " not a list"

class BDecodeMaybe a where
    bdecodeMaybe :: Maybe B -> Either String a

instance BDecode a => BDecodeMaybe (Maybe a) where
    bdecodeMaybe (Just v) = Just <$> bdecode v
    bdecodeMaybe Nothing = Right Nothing 

instance {-# overlappable #-} BDecode a => BDecodeMaybe a where
    bdecodeMaybe (Just v) = bdecode v
    bdecodeMaybe Nothing = Left "decoding a non-maybe from Nothing" 

instance {-# overlappable #-} (All2 BDecodeMaybe (Code a), HasDatatypeInfo a) => BDecode a where
    bdecode = gbdecode pure 
    
gbdecode :: forall a
          . (All2 BDecodeMaybe (Code a), HasDatatypeInfo a)
         => (Text -> Maybe Text)
         -> B
         -> Either String a

gbdecode mp b = fmap to $ msum $ hcollapse $ hcliftA2
                do Proxy :: Proxy (All BDecodeMaybe)
                do decodeConstructor  mp b 
                do constructorInfo $ datatypeInfo (Proxy :: Proxy a) 
                do injections :: NP (Injection (NP G.I) (Code a)) (Code a) -- necessaria!

            -- Nothing -> Left $ "field " <> name <> " not found"

decodeConstructor  
    :: forall (a' :: [[Type]]) (a :: [Type]) 
    .  All BDecodeMaybe a 
    => (Text -> Maybe Text)
    -> B  -- b to consume
    -> ConstructorInfo a -- the constructor to use
    -> Injection (NP G.I) a' a -- how to promote a constructor to its type
    -> K (Either String  (SOP G.I a')) a
decodeConstructor p  (D m) (Record _ ns) (Fn inj) = K $ fmap (SOP . unK  . inj) $ hsequence $  hcliftA
    do Proxy :: Proxy BDecodeMaybe
    do \(FieldInfo name)  -> bdecodeMaybe $ p (T.pack name) >>=  \k -> M.lookup k m 
    ns

decodeConstructor _ _ _ _ = K $ Left "couldn't reconstruct your toy"
