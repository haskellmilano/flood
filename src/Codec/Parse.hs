
{-# LANGUAGE OverloadedStrings      #-}

module Codec.Parse where

import           Data.Attoparsec.ByteString.Char8
                                                ( Parser
                                                , char
                                                , decimal
                                                , parseOnly
                                                , signed
                                                )
import qualified Data.Attoparsec.ByteString.Char8
                                               as P
import           Control.Applicative
import           Control.Lens                   ( left' )
import           Control.Monad
import           Data.ByteString                ( ByteString )
import qualified Data.ByteString.Char8         as BC
import qualified Data.Map.Strict                      as M
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Data.Text.Encoding             ( decodeUtf8'
                                                , encodeUtf8
                                                )
import           Codec.B
import Prelude
consume :: Char -> Parser ()
consume = void . char

liftRight :: Show e => Either e a -> Parser a
liftRight = either (fail . show) pure


parse :: ByteString -> Either Text B
parse bs = left' T.pack $ parseOnly parser bs


render :: B -> ByteString
render (S x ) = showB (BC.length x) <> ":" <> x
render (I i ) = "i" <> showB i <> "e"
render (L xs) = "l" <> foldr ((<>) . render) "" xs <> "e"
render (D m ) = "d" <> foldr f "" (M.assocs m) <> "e"
    where f (k, v) a = render (S $ encodeUtf8 k) <> render v <> a

showB :: (Show a) => a -> ByteString
showB = BC.pack . show

parser :: Parser B
parser = parserS <|> parserI <|> parserL <|> parserD

parserS :: Parser B
parserS = S <$> parserByteString

parserByteString :: Parser ByteString
parserByteString = do
    l <- decimal
    consume ':'
    P.take l

parserText :: Parser Text
parserText = parserByteString >>= liftRight . decodeUtf8'

parserI :: Parser B
parserI = do
    consume 'i'
    i <- signed decimal
    consume 'e'
    pure $ I i

parserL :: Parser B
parserL = do
    consume 'l'
    elements <- many parser
    consume 'e'
    pure $ L elements

parserD :: Parser B
parserD = do
    consume 'd'
    elements <- many $ (,) <$> parserText <*> parser
    consume 'e'
    pure $ D $ M.fromList elements

