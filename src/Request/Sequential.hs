{-# LANGUAGE BlockArguments      #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE NoImplicitPrelude   #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Strict              #-}

module Request.Sequential where

import           Control.Lens
import           Control.Monad.Trans.State.Strict
import qualified Data.Map.Strict                  as M
import qualified Data.Set                         as S
import           Data.Time
import           Peer.Peer
import           Peer.Protocol
import           Pipes                            as P
import           Pipes.Core
import           Pipes.Lift
import           Protolude                        hiding (StateT, find, get,
                                                   put, runStateT)
import           Torrent.State
import           Torrent.Taint




-- cleanTaint  :: UTCTime -> 

-- | dumb round robin assign requests to peers
sequentialRequests
    :: Monad m 
    => UTCTime 
    -> TorrentData
    -> Producer (Peered Protocol) m ()
sequentialRequests now  td@TorrentData {..} = evalStateP initial_pieces $ 
    P.each (M.assocs _td_peers_have) //> \(peer, pieces) -> when 
        do _td_chockedBy ^? ix peer == Just Unchoked 
                &&
                S.notMember peer (uploadingPeers initial_pieces)
        do  intervals <- lift get
            case find intervals (S.toList pieces) of 
                Nothing -> pure ()
                Just pieceIndex -> do 
                    maybeInterval <- lift $ join <$> updateIn pieceIndex (markFirstRequested now peer)
                    P.each maybeInterval //> \(begin,end) ->
                        let _begin = fromIntegral begin
                            _finish = fromIntegral end
                        in yield $ Peered peer $ Request pieceIndex _begin (_finish - _begin)
    where initial_pieces = td ^. td_taints

-- | compute the set of peers engaged in a request
uploadingPeers :: Map TaintIndex Taint -> Set PeerId
uploadingPeers = foldMap (foldMap (S.singleton . peered_peer) . _requested)

-- | finds the first PieceIndex which has missings
find :: Map TaintIndex Taint -> [TaintIndex] -> Maybe TaintIndex
find _ [] = Nothing
find m (x:xs) = 
    do  t <- M.lookup x m
        guard $ hasMissing t 
        pure x
    <|> find m xs


updateIn:: (Monad m ,Ord k) => k -> StateT v m a -> StateT (M.Map k v) m (Maybe a) 
updateIn k innerState = do
    m <- get
    traverse 
        do \v -> do (a, v') <- lift $ runStateT innerState v
                    put (M.insert k v' m) $> a
        do M.lookup k m 