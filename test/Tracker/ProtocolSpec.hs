{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Tracker.ProtocolSpec where

-- import           Bencoding

import Bencoding
import Client
import Common(fileContentsTracker, shouldBeRight)
import Control.Lens
import Control.Monad
import Peer.Peer
import Protolude
import System.FilePath
import Test.Hspec
import Test.QuickCheck.Instances.ByteString ()
import Test.QuickCheck.Instances.Text ()
import Torrent.Encoding
import Torrent.State
import Tracker.Protocol

p0 :: Peer
p0 = Peer "127.0.0.1" 8000 Nothing

spec :: Spec
spec = describe "tracker protocol" $ do
  files <- runIO fileContentsTracker
  forM_ files $ \(fn, f) -> do
    it
      do "should decode meta_torrent from file: " <> takeFileName fn
      do parseMetaTorrent f `shouldSatisfy` has _Right
    it
      do "can retrieve the swarm from tracker for torrent: " <> takeFileName fn
      do
        meta_torrent <- shouldBeRight $ parseMetaTorrent f
        let client' = Client "01234567890123456789" "localhost" 6881 1024
        case _meta_torrent_info meta_torrent of
          SingleTorrent _name _pl _ps l _ms -> do
            let torrentState = TorrentState 0 0 l
                request =
                  TrackerRequest client' torrentState (Just Started)
            response <- getSwarm meta_torrent request
            response `seq` pure ()
          MultiTorrent _name _pl _ps _fs -> do
            let torrentState = TorrentState 0 0 0
                request =
                  TrackerRequest client' torrentState (Just Started)
            response <- getSwarm meta_torrent request
            response `seq` pure ()
  it
    do "encode-decode no swarms"
    do
      shouldBe
        do
          bdecode $ bencode $ Swarm
            do Nothing
            do Nothing
            do Nothing
            do Nothing
        do
          Right $ Swarm
            do Nothing
            do Nothing
            do Nothing
            do Nothing
  it
    do "encode-decode one peer swarms"
    do
      shouldBe
        do
          bdecode $ bencode $ Swarm
            do Just [p0]
            do Nothing
            do Nothing
            do Nothing
        do
          Right $ Swarm
            do Just [p0]
            do Nothing
            do Nothing
            do Nothing
