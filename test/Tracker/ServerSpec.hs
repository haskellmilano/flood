{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Tracker.ServerSpec where

import Client
import Control.Concurrent.STM
import Control.Lens
import Peer.Peer
import Protolude
import Test.Hspec
import Test.QuickCheck.Instances.ByteString ()
import Test.QuickCheck.Instances.Text ()
import Torrent.Encoding  
import Torrent.State
import Tracker.Protocol
import Tracker.Server


p0 :: Peer
p0 = Peer "127.0.0.1" 8082 Nothing

p1 :: Peer
p1 = Peer "127.0.0.1" 8083 Nothing

sortPeers :: Swarm -> Swarm
sortPeers = over swarm_peers $ fmap sort

swarm :: [Peer] -> Swarm
swarm ps = Swarm
  do Just ps
  do Nothing
  do Nothing
  do Nothing

trackerRequest :: Peer -> IO TrackerRequest
trackerRequest (Peer ip port id) = do
  pid <- maybe generatePeerId pure  id
  pure $ TrackerRequest
    do
      Client
        do pid
        do ip
        do fromIntegral port
        do 16384
    do
      TorrentState
        0
        0
        0
    do Nothing

torrent :: MetaTorrent
torrent = MetaTorrent
  "http://127.0.0.1:8081"
  do
    SingleTorrent
      do "test.torrent"
      do 0
      do ""
      do 0
      do Nothing
  do Nothing
  do Nothing
  do Nothing
  do Nothing
  do Nothing

{-# ANN module ("HLint: ignore Reduce duplication" :: Text) #-}

spec :: Spec
spec = do
  resetC <- runIO newTChanIO
  restartC <- runIO newTChanIO
  runIO $ atomically $ writeTChan restartC ()
  let reset = do
        atomically $ writeTChan resetC ()
  describe "tracker server"
    $ before_ do
      atomically $ readTChan restartC
      void $ forkIO $ serverUp resetC restartC
    $ after_ reset
    $ do
      it
        do "should serve back the emptySwarm"
        do
          rq <- trackerRequest p0
          swarm0 <- getSwarm torrent rq
          swarm0 `shouldBe` emptySwarm
      it
        do "should serve back the  singleton swarm"
        do
          rq0 <- trackerRequest p0
          _ <- getSwarm torrent rq0
          rq1 <- trackerRequest p1
          swarm0 <- getSwarm torrent rq1
          swarm0 `shouldBe` swarm [p0]
      it
        do "should not serve duplicate ip's"
        do
          rq0 <- trackerRequest p0
          _ <- getSwarm torrent rq0
          _ <- getSwarm torrent rq0
          rq1 <- trackerRequest p1
          swarm0 <- getSwarm torrent rq1
          swarm0 `shouldBe` swarm [p0]
      it
        do "should serve multiple ip's"
        do
          rq0 <- trackerRequest p0
          _ <- getSwarm torrent rq0
          rq1 <- trackerRequest p1
          _ <- getSwarm torrent rq1
          swarm0 <- getSwarm torrent rq0
          sortPeers swarm0 `shouldBe` sortPeers (swarm [p0, p1])
          swarm1 <- getSwarm torrent rq0
          sortPeers swarm1 `shouldBe` sortPeers (swarm [p0, p1])
