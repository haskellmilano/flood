{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Strict #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-partial-fields #-}

module Request.SequentialSpec where

import Control.Lens
import Control.Monad.State.Strict
import qualified Data.Map as M
import Data.Time
import Peer.Peer
import Peer.Protocol hiding (Presence)
import Pipes.Prelude as P
import Protolude hiding (execState)
import Request.Sequential
import Test.Hspec
import Test.QuickCheck.Instances.ByteString ()
import Test.QuickCheck.Instances.Text ()
import Torrent.Encoding
import Torrent.Info
import Torrent.State
import Torrent.Taint
import Common

f0 :: TorrentFile
f0 = TorrentFile 202 ["test.iso"] mempty

t0 :: TorrentInfo
t0 = mkTorrent "test" (repeat "") "" 100 (M.singleton 0 f0)


requests ::
  (Map TaintIndex Taint -> Map TaintIndex Taint) ->
  Map PeerId (Set TaintIndex) ->
  Map PeerId ChokeState ->
  [Peered Protocol]
requests f haves chokes = P.toList $ sequentialRequests now0 td
  where
    td0 = over td_taints f $ newTorrentData 10 t0
    td = td0 {_td_peers_have = haves, _td_chockedBy = chokes}

now0 :: UTCTime
now0 = UTCTime (toEnum 0) 0

spec :: Spec
spec = do
  describe "requests are computed" $ do

    it
      do "starting a torrent with one peer"
      do
        requests
          do identity
          do [(p0, [0 .. 2])] -- haves
          do [(p0, Unchoked)] -- chokes
          `shouldBe` do
            [ Peered p0 $ Request 0 0 10
              ]
    it
      do "haves are seen"
      do
        requests
          do identity
          do [(p0, [1, 2])] -- haves
          do [(p0, Unchoked)] -- chokes
          `shouldBe` do
            [ Peered p0 $ Request 1 0 10
              ]
    it
      do "choked are respected "
      do
        requests
          do identity
          do [(p0, [0 .. 2])] -- haves
          do [(p0, Choked)] -- chokes
          `shouldBe` do []
    it
      do "choked are default  "
      do
        requests
          do identity
          do [(p0, [0 .. 2])] -- haves
          do [] -- chokes
          `shouldBe` do []

    it
      do "busy peers are excluded"
      do
        requests
          do ix 0 %~ markRequested now0 0 p0 -- requested
          do [(p0, [0 .. 2])] -- haves
          do [(p0, Unchoked), (p1, Unchoked)] -- chokes
          `shouldBe` do []
    it
      do "requested subpieces are excluded"
      do
        requests
          do ix 0 %~ markRequested now0 0 p0 -- requested
          do [(p0, [0 .. 2]), (p1, [0])] -- haves
          do [(p0, Unchoked), (p1, Unchoked)] -- chokes
          `shouldBe` do
            [ Peered p1 $ Request 0 10 10
              ]
    it
      do "downloaded subpieces are excluded"
      do
        requests
          do ix 0 %~ markDownloaded 0 . markRequested now0 0 p0 -- downloaded
          do [(p0, [0 .. 2])] -- haves
          do [(p0, Unchoked)] -- chokes
          `shouldBe` do
            [ Peered p0 $ Request 0 10 10
              ]
    it
      do "full downloaded pieces are excluded"
      do
        requests
          do ix 0 %~ execState (markAllRequested now0 p0 >> markAllDownloaded) -- downloaded
          do [(p0, [1, 2])] -- haves
          do [(p0, Unchoked)] -- chokes
          `shouldBe` do
            [ Peered p0 $ Request 1 0 10
              ]
    it
      do "full (requested + downloaded) are excluded"
      do
        requests
          do
            ix 1 %~ execState
              do
                forM_ @[] [0, 10 .. 40] $ \i -> markRequestedS now0 i p0
                forM_ @[] [50, 60 .. 90] $ \i -> markRequestedS now0 i p0 >> markDownloadedS i
          do [(p0, [1 .. 2]), (p1, [1, 2])] -- haves
          do [(p0, Unchoked), (p1, Unchoked)] -- chokes
          `shouldBe` do
            [ Peered p1 $ Request 2 0 2
              ]
    it
      do "multiple peers are handled"
      do
        requests
          do ix 0 %~ markDownloaded 0 . markRequested now0 0 p0 -- downloaded
          do [(p0, [0 .. 2]), (p1, [2])] -- haves
          do [(p0, Unchoked), (p1, Unchoked)] -- chokes
          `shouldBe` do
            [ Peered p0 $ Request 0 10 10,
              Peered p1 $ Request 2 0 2
              ]

-- it  do "multiple files are handled"
--     do
--         it  do "partial last piece is handled"
--             do requests
--                 do [(2,Taint [(0,1,p0,  Downloaded)] 2)] -- downloaded
--                 do [(p0,[2])] -- haves
--                 do [(p0,Unchoked)] -- chokes
--                 do [] -- busy
--                 `shouldBe`
--                 do [ Peered p0 $ Request 2 1 1]
