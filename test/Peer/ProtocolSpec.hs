{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Peer.ProtocolSpec where

-- import           Bencoding

import Client
import Control.Logging
import Data.Attoparsec.ByteString
import qualified Data.ByteString as BS
import Data.Word
import Peer.Protocol
import Pipes
import qualified Pipes.Prelude as P
import Protolude
import Test.Hspec
import Test.Hspec.QuickCheck
import Test.QuickCheck
import Test.QuickCheck.Instances.ByteString ()
import Test.QuickCheck.Instances.Text ()
import Torrent.Taint

-- import Text.Pretty.Simple

newtype Limited = Limited {limitedBytes :: [Word8]} deriving (Show)

instance Arbitrary Limited where
  arbitrary = Limited <$> (choose (14, 100) >>= vector)

instance Arbitrary TaintIndex where
  arbitrary = TaintIndex . fromIntegral <$> (arbitrary :: Gen Word32)

instance Arbitrary Protocol where
  arbitrary = do
    l <- choose (1, 10 :: Int)
    case l of
      1 -> pure KeepAlive
      2 -> pure Choke
      3 -> pure UnChoke
      4 -> pure Interested
      5 -> pure NotInterested
      6 -> BitField . parseBitfield . BS.pack . limitedBytes <$> arbitrary
      7 -> Have <$> arbitrary
      8 -> Request <$> arbitrary <*> arbitrary <*> arbitrary
      9 -> Piece <$> arbitrary <*> arbitrary <*> (BS.pack <$> (choose (14, 1024) >>= vector))
      10 -> Cancel <$> arbitrary <*> arbitrary <*> arbitrary
      _ -> panic "parsing protocol failed"

spec :: Spec
spec =
  describe "peer_protocol" $ do
    it
      do "should roundtrip bitfield parse and render"
      do
        shouldBe
          do renderBitfield . parseBitfield $ BS.pack [0 .. 10]
          do BS.pack [0 .. 10]
    prop
      do "should roundtrip bitfield parse and render"
      do
        \(Limited xs) ->
          do (renderBitfield . parseBitfield) $ BS.pack xs
            == do BS.pack xs
    prop
      do "should roundtrip protocol parse and render"
      do
        \x ->
          do parseOnly parseProtocol $ renderProtocol x
            == do Right x
    prop
      do "incremental parser should wait for handshake and then parse all"
      do
        \x -> withStdoutLogging $ do
          setLogLevel LevelWarn
          peerId <- generatePeerId
          let bs = renderProtocol x
          let hs = renderHandshake infoHash peerId
              input = do
                yield hs
                yield $ BS.take 20 bs
                yield $ BS.drop 20 bs
                yield (renderProtocol x <> renderProtocol x)
          -- peer = Peer (BS.singleton 1) 99 Nothing
          (peer', remaining) <- parseAll (Just peerId) input
          peer' `shouldBe` peerId
          Just x' <- P.head remaining
          x `shouldBe` x'

infoHash :: ByteString
infoHash = BS.replicate 20 1

chunk :: Int -> BS.ByteString -> [BS.ByteString]
chunk _ "" = []
chunk n x =
  let (y, x') = BS.splitAt n x
   in y : chunk n x'
