{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}

module Torrent.TaintSpec where

import Client
import Control.Monad.State.Strict
import qualified Data.Map as IM
import Data.Time
import GHC.IO (unsafePerformIO)
import Peer.Peer
import Test.Hspec
import Torrent.Taint
import Prelude

now0 :: UTCTime
now0 = UTCTime (toEnum 0) 0

p0 :: PeerId
p0 = unsafePerformIO generatePeerId

spec :: Spec
spec = do
  describe "taint structure" $ do
    let offsets = [0 .. 99 :: PieceOffset]
        initial = mkTaint 1 100
    it "should boot incomplete" $ do
      isComplete initial `shouldBe` False
    it "allow all initial pieces to be requested" $ do
      let final = execState (markAllRequested now0 p0) initial
      forM_ offsets (\i -> isRequested i final `shouldBe` True)
    it "allow all initial pieces to be requested and then downloadedes, resulting in a complete piece" $ do
      isComplete (execState (markAllRequested now0 p0 >> markAllDownloaded) initial) `shouldBe` True
    it "selects first missing and mark it as requested" $
      do
        execState (markFirstRequested now0 p0) initial
        `shouldBe` Taint
          do IM.fromList [(x, PieceOffset $ pid + 1) | x@(PieceOffset pid) <- tail offsets]
          do [(0, Peered p0 (1, now0))]
          do []
