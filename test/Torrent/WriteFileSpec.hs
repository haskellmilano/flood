{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Torrent.WriteFileSpec where

import Client
import Control.Lens hiding (each)
import Control.Logging
import Crypto.Hash.SHA1 as SHA1
import qualified Data.ByteString as BS
import qualified Data.Map.Strict as M
import Data.Time
import GHC.IO (unsafePerformIO)
import Lib
import Peer.Peer
import Peer.Protocol
import Pipes
import Pipes.Lift
import Pipes.Safe
import Protolude
import System.Directory
import System.FilePath
import Test.Hspec
import Test.QuickCheck.Instances.ByteString
  (
  )
import Test.QuickCheck.Instances.Text ()
import Torrent.Encoding
import Torrent.Info
import Torrent.State
import Torrent.Taint
import Torrent.Write

{-# ANN module ("HLint: ignore Reduce duplication" :: Text) #-}

p0 :: PeerId
p0 = unsafePerformIO generatePeerId

p1 :: PeerId
p1 = unsafePerformIO generatePeerId

infoTest :: FilePath -> FileLength -> [(TaintHash, TaintLength, TaintCoordinate)] -> TorrentInfo
infoTest name l validHashes =
  TorrentInfo (toS name) 10 "" (mkIdxMap 0 validHashes) (M.singleton 0 (TorrentFile l [name] Nothing))

subPieceSize :: PieceLength
subPieceSize = 4

now0 :: UTCTime
now0 = UTCTime (toEnum 0) 0

testDriver ::
  FileLength ->
  [(TaintHash, TaintLength, TaintCoordinate)] ->
  -- | requests
  [(TaintIndex, PeerId, PieceOffset)] ->
  -- | downloads
  [(TaintIndex, PeerId, PieceOffset)] ->
  -- | received
  [Peered Protocol] ->
  IO ([ByteString], Map TaintIndex Taint)
testDriver size validHashes = testDriverMulti $ infoTest "test.txt" size validHashes

testDriverMulti ::
  TorrentInfo ->
  -- | requests
  [(TaintIndex, PeerId, PieceOffset)] ->
  -- | downloads
  [(TaintIndex, PeerId, PieceOffset)] ->
  -- | received
  [Peered Protocol] ->
  IO ([ByteString], Map TaintIndex Taint)
testDriverMulti torrent requesteds downloadeds receiveds =
  withStdoutLogging $ do
    setLogLevel LevelError
    let patch = over td_taints $ foldl' (.) identity $
          do
            fmap
              do
                \(idx, peer, start) ->
                  M.adjust (markRequested now0 start peer) idx
              do requesteds
            <> do
              fmap
                do
                  \(idx, peer, start) ->
                    M.adjust (markDownloaded start . markRequested now0 start peer) idx
                do downloadeds
        torrentData = patch $ newTorrentData subPieceSize torrent

    finalTorrentData <-
      runSafeT $ runEffect $ execStateP torrentData $
        each receiveds
          >-> writeToFile torrent "."
    let basePath = base_folder torrent
        filePaths = (</>) basePath . torrent_filePath <$> M.elems (torrent_files torrent)
    content <- traverse BS.readFile filePaths
    traverse_ removeFile filePaths
    pure (content, finalTorrentData ^. td_taints)

spec :: Spec
spec = describe "write file consumer" do
  -- it "creates empty (1GB) file" $
  --     withStdoutLogging $ do
  --         zeroFile 1_000_000 "test.empty"
  --         l <- getFileSize "test.empty"
  --         removeFile "test.empty"
  --         shouldBe l 1_000_000
  it "streaming-fills from 0" do
    ([content], tainteds) <- testDriver
      do 20 -- file length -> 2 pieces
      do [("", 10, TaintCoordinate 0 0), ("", 10, TaintCoordinate 0 10)]
      do [(0, p0, 0)] -- requesteds
      do [] -- downloads
      do [Peered p0 $ Piece 0 0 "ciao"]

    shouldBe content "ciao"
    tainteds
      `shouldBe` [ ( 0,
                     markDownloaded 0 $ markRequested now0 0 p0 $
                       mkTaint subPieceSize 10
                   ),
                   (1, mkTaint subPieceSize 10)
                 ]
  it "streaming-fills out of order" do
    ([content], tainteds) <- testDriver
      do 10 -- file length -> 1 piece
      do [("", 10, TaintCoordinate 0 0)]
      do [(0, p0, 0), (0, p0, 4)] -- requesteds
      do [] -- downloads
      do [Peered p0 $ Piece 0 0 "ciao", Peered p0 $ Piece 0 2 "ao ma"]

    shouldBe content "ciao"
    shouldBe
      tainteds
      [ ( 0,
          markRequested now0 4 p0 $ markDownloaded 0
            $ markRequested now0 0 p0
            $ mkTaint subPieceSize 10
        )
      ]
  it "drop unrequested pieces" do
    ([content], tainteds) <- testDriver
      do 10 -- file length
      do [("", 10, TaintCoordinate 0 0)]
      do [(0, p0, 0)] -- requesteds
      do [] -- downloads
      do
        [ Peered p0 $ Piece 0 1 "ciao"
          ]
    shouldBe content ""
    shouldBe tainteds [(0, markRequested now0 0 p0 $ mkTaint subPieceSize 10)]
  it "drop out-of-size pieces" do
    ([content], tainteds) <- testDriver
      do 10 -- file length
      do [("", 10, TaintCoordinate 0 0)]
      do [(0, p0, 0)] -- requesteds
      do [] -- downloads
      do
        [ Peered p0 $ Piece 0 0 "ciaoo",
          Peered p0 $ Piece 0 0 "cia"
          ]
    shouldBe content ""
    shouldBe tainteds [(0, markRequested now0 0 p0 $ mkTaint 4 10)]
  it "full downloaded pieces are removed from state" do
    ([content], tainteds) <- testDriver
      do 10 -- file length
      do [(TaintHash $ SHA1.hash "ciaosonoio", 10, TaintCoordinate 0 0)]
      do [(0, p0, 0), (0, p0, 4), (0, p0, 8)] -- requesteds
      do [] -- downloads
      do Peered p0 <$> zipApply [Piece 0 0, Piece 0 8, Piece 0 4] ["ciao", "io", "sono"]
    shouldBe content "ciaosonoio"
    shouldBe tainteds []
  it "should download multi-file torrents correctly" do
    let fileContents = ["ciaosonoio", "5678sonoio", "1234sonoio"]
        validHashes = do
          (content, fileName) <- zip fileContents [0 ..]
          pure (TaintHash $ SHA1.hash content, 10, TaintCoordinate fileName 0)
        fileNames :: [FilePath] = show <$> [0 :: Int .. 2]
        torrent =
          TorrentInfo
            "multi-file"
            10
            ""
            (mkIdxMap 0 validHashes)
            (mkIdxMap 0 $ (\name -> TorrentFile 10 [name] Nothing) <$> fileNames)
        taintRequests :: TaintIndex -> [(TaintIndex, PeerId, PieceOffset)]
        taintRequests idx = [0, 4, 8] <&> (idx,p0,)

    (contentFromFiles, _) <-
      testDriverMulti
        do torrent
        do [0 .. 2] >>= taintRequests
        do [] -- downloads
        do
          Peered p0
            <$> zipApply
              ([0 .. 3] >>= \i -> [Piece i 0, Piece i 4, Piece i 8])
              ["ciao", "sono", "io", "5678", "sono", "io", "1234", "sono", "io"]
    contentFromFiles `shouldBe` fileContents

zipApply :: [a -> b] -> [a] -> [b]
zipApply fs as = (\(f, a) -> f a) <$> zip fs as
