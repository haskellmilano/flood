{-# LANGUAGE OverloadedLists #-}

module Torrent.StateSpec where

import Control.Lens
import Lib
import Test.Hspec
import Test.QuickCheck.Instances.ByteString
  (
  )
import Test.QuickCheck.Instances.Text ()
import Torrent.Encoding  
import Torrent.Taint
import Torrent.Info 
import Prelude
import Protolude (ByteString)


mkFalseHash :: Int -> ByteString -> TaintHash
mkFalseHash n  = TaintHash . mconcat . replicate (n*20)

multiInfo :: TorrentEncoding
multiInfo =
  MultiTorrent
    "test-torrent"
    4
    (unTaintHash $ mkFalseHash 5 "a")
    [ TorrentFileEncoding 8 ["1"] Nothing,
      TorrentFileEncoding 12 ["2"] Nothing
    ]

torrentFile :: FileLength -> TorrentFile
torrentFile len = TorrentFile len mempty Nothing

simpleTorrent :: TaintLength -> [FileLength] -> TorrentInfo
simpleTorrent taintLen fileLengths =
  mkTorrent "test" (repeat "") "" taintLen $ mkIdxMap 0 (torrentFile <$> fileLengths)

taintFileLength :: TorrentInfo  -> TaintIndex -> Maybe FileLength
taintFileLength t i = do
  (_, _, TaintCoordinate fid _) <- torrent_taints t ^? ix i
  torrentFile_length <$> torrent_files t ^? ix fid

spec :: Spec
spec = describe "torrentState" $ do
  it "multi file should be decoded correctly" $ do
    fromTorrentEncoding multiInfo `shouldBe` do
      TorrentInfo
        { torrent_name = "test-torrent",
          torrent_taint_length = TaintLength 4,
          torrent_hash = "6\176i\199\208\227\196\142\229\153\199\220S\a\178\&4\235\249\159w",
          torrent_taints =
            [ (TaintIndex 0, (mkFalseHash 1 "a", TaintLength 4, TaintCoordinate (FileIndex 0) (FileOffset 0))),
              (TaintIndex 1, (mkFalseHash 1 "a", TaintLength 4, TaintCoordinate (FileIndex 0) (FileOffset 4))),
              (TaintIndex 2, (mkFalseHash 1 "a", TaintLength 4, TaintCoordinate (FileIndex 1) (FileOffset 0))),
              (TaintIndex 3, (mkFalseHash 1 "a", TaintLength 4, TaintCoordinate (FileIndex 1) (FileOffset 4))),
              (TaintIndex 4, (mkFalseHash 1 "a", TaintLength 4, TaintCoordinate (FileIndex 1) (FileOffset 8)))
            ],
          torrent_files =
            [ ( FileIndex 0,
                TorrentFile
                  { torrentFile_length = FileLength 8,
                    torrentFile_path = ["1"],
                    torrentFile_md5sum = Nothing
                  }
              ),
              ( FileIndex 1,
                TorrentFile
                  { torrentFile_length = FileLength 12,
                    torrentFile_path = ["2"],
                    torrentFile_md5sum = Nothing
                  }
              )
            ]
        }
  it "should get file correctly from taintIndex" $ do
    let torrent = simpleTorrent 4 [8, 12]
    taintFileLength torrent 0 `shouldBe` Just 8
    taintFileLength torrent 1 `shouldBe` Just 8
    taintFileLength torrent 2 `shouldBe` Just 12
    taintFileLength torrent 3 `shouldBe` Just 12
    taintFileLength torrent 4 `shouldBe` Just 12

{-  it "should compute taint size correctly" $ do
   taintFileSize 4 (torrentFile 12 ) `shouldBe` 3
   taintFileSize 4 (torrentFile 13 ) `shouldBe` 4
   taintFileSize 4 (torrentFile 15 ) `shouldBe` 4
   taintFileSize 4 (torrentFile 1 ) `shouldBe` 1 -}
