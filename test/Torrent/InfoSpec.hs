{-# LANGUAGE BlockArguments #-}

module Torrent.InfoSpec where

import           Bencoding
import           Common
import           Control.Monad
import           Control.Lens
import           System.FilePath
import           Test.Hspec
import           Test.QuickCheck.Instances.ByteString ()
import           Test.QuickCheck.Instances.Text       ()
import           Torrent.Encoding  
import Prelude

-- import Data.Text
-- import qualified Data.Text.Lazy as T
-- import Text.Pretty.Simple

spec :: Spec
spec = describe "torrentInfo" $ do
    files <- runIO fileContents -- real life examples
    forM_ files $ \(fn,f) -> do
        it 
            do  "should decode meta_torrent from file: " <> takeFileName fn
            do  parseMetaTorrent f  `shouldSatisfy` has _Right
        it  do "roundtrips torrent and bytestrings of file: " <> takeFileName fn
            do  MetaTorrent _ i _ _ _ _ _ <- shouldBeRight $ parseMetaTorrent f
                shouldBe
                    do bdecode . bencode $ i
                    do Right i


    -- let rt = fmap splitHash . bdecode . bencode . mconcat $ i 
    -- pPrint rt
    -- Right i `shouldBe` rt

