module Common where

import Data.ByteString as BS hiding (zip)
import Prelude
import Peer.Peer

-- {-# ANN module "HLint: ignore Redundant $" #-}
-- utile per commentare gli elementi di una lista compresi gli estremi
cns :: a -> [a] -> [a]
cns = (:)

fileContents :: IO [(FilePath, BS.ByteString)]
fileContents = zip files <$> traverse BS.readFile files
  where
    files =
      cns "test/data/test2.torrent"
        $ cns "test/data/ubuntu-19.10-desktop-amd64.iso.torrent"
        $ cns "test/data/Fedora-Cinnamon-Live-x86_64-31.torrent"
        $ cns "test/data/archlinux-2020.02.01-x86_64.iso.torrent"
        $ cns "test/data/kubuntu-19.10-desktop-amd64.iso.torrent"
        $ cns "test/data/Fedora-Astronomy_KDE-Live-x86_64-31.torrent"
        $ []

fileContentsTracker :: IO [(FilePath, BS.ByteString)]
fileContentsTracker = zip files <$> traverse BS.readFile files
  where
    files =
      --  "test/data/test2.torrent" $ -- UDP announce not implemented
      cns "test/data/ubuntu-19.10-desktop-amd64.iso.torrent"
        $ cns "test/data/Fedora-Cinnamon-Live-x86_64-31.torrent"
        $ cns "test/data/kubuntu-19.10-desktop-amd64.iso.torrent"
        $ cns "test/data/archlinux-2020.02.01-x86_64.iso.torrent"
        $ cns "test/data/Fedora-Astronomy_KDE-Live-x86_64-31.torrent"
        $ []

-- isRight :: Either a b -> Bool
-- isRight (Right _) = True
-- isRight _         = False

shouldBeRight :: (MonadFail f, Show a1) => Either a1 a2 -> f a2
shouldBeRight (Right x) = pure x
shouldBeRight (Left x) = fail $ show x


p0:: PeerId 
p0 = PeerId {unpeer_id = "-flood-p0gaukzdqvuom"}
p1 :: PeerId
p1 = PeerId {unpeer_id = "-flood-p1xjleepojqzt"}