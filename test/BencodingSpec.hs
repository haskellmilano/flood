{-# LANGUAGE BlockArguments    #-}
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeFamilies      #-}
{-# options_ghc -Wno-partial-fields #-}

module BencodingSpec where

import           Bencoding
import           Data.ByteString                ( ByteString )
import           Data.Map.Strict
import           Test.Hspec
import           Common
import Control.Lens
import           Generics.SOP.TH
import           Test.Hspec.QuickCheck
import           Test.QuickCheck
import           Test.QuickCheck.Instances.ByteString
                                                ( )
import           Test.QuickCheck.Instances.Text ( )
import           Prelude

-- import           Text.Pretty.Simple

-- test structure for generics
data T a = T
        { t_prefix_field_1 :: Integer
        , t_prefix_field_2 :: Maybe [[ByteString]]
        , t_prefix_field_poly :: a
        } | T2  
        { t_prefix_field_4 :: ByteString
        , t_prefix_field_2 :: Maybe [[ByteString]]
        , t_prefix_field_poly :: a
        } 
    deriving (Show, Eq)

-- from Generics.SOP.TH
deriveGeneric ''T

i' :: Integer -> Integer
i' n = n :: Integer

b' :: ByteString -> ByteString
b' x = x :: ByteString

-- test stripping works
instance BDecode a => BDecode (T a) where
    bdecode = gbdecode (stripLabel "t_prefix_") 

instance (BEncode a) => BEncode (T a) where
    bencode = gbencode (stripLabel "t_prefix_")

roundTripWorks :: (Eq b, BDecode b, BEncode b) => b -> Bool
roundTripWorks x = roundTrip x == Right x

spec :: Spec
spec = describe "bencoding" $ do
    it "should parse strings" $ shouldBe (parse "4:spam") (Right (S "spam"))
    it "should parse integers" $ shouldBe
        (parse "i191082039812093810298301928301928321093e")
        (Right (I 191082039812093810298301928301928321093))
    it "should parse lists"
        $ shouldBe (parse "l4:spam4:eggse") (Right (L [S "spam", S "eggs"]))
    it "should parse dictionaries" $ shouldBe
        (parse "d3:cow3:moo4:spami82ee")
        (Right $ D $ fromList [("cow", S "moo"), ("spam", I 82)])
     -- PROPS
    prop "should render-parse int" $ \i -> case (parse . render . I) i of
        Right (I j) -> j == i
        _           -> False
    prop "should render-parse string" $ \s -> case (parse . render . S) s of
        Right (S j) -> s == j
        _           -> False
    prop "should render-parse list" $ \l ->
        case (parse . render . L) (arbB <$> l) of
            Right (L l') -> (arbB <$> l) == l'
            _            -> False
    prop "should render-parse arbitrary values" $ \s ->
        case (parse . render) (arbB s) of
            (Right j) -> j == arbB s
            _         -> False
    contents <- (snd <$>) <$> runIO fileContents
    it "should parse torrent files"
        $               traverse parse contents
        `shouldSatisfy` has _Right
    it "should parse-render torrent files"
        $ all ((==) <$> Right <*> fmap render . parse) contents
    it "should strip 't_prefix_' from the generic bencoding of T values"
        $  bencode do
               T 14 (Just [["ciao"]]) $ T 42 (Just [["mamma"]]) $ i' 42
        == D
               [ ("field 2", L [L [S "ciao"]])
               , ( "field poly"
                 , D
                     [ ("field 2"   , L [L [S "mamma"]])
                     , ("field poly", I 42)
                     , ("field 1"   , I 42)
                     ]
                 )
               , ("field 1", I 14)
               ]
    it "should strip 't_prefix_' from the generic bencoding of T2 values"
        $  bencode do
               T2 "ahi" (Just [["ciao"]]) $ T 42 (Just [["mamma"]]) $ i' 42
        == D
               [ ("field 2", L [L [S "ciao"]])
               , ( "field poly"
                 , D
                     [ ("field 2"   , L [L [S "mamma"]])
                     , ("field poly", I 42)
                     , ("field 1"   , I 42)
                     ]
                 )
               , ("field 4", S "ahi")
               ]
    it "should handle Nothing in T values"
        $  bencode (T 14 Nothing $ T 42 (Just [["mamma"], ["ciao"]]) $ i' 42)
        == D
               [ ( "field poly"
                 , D
                     [ ("field 2"   , L [L [S "mamma"], L [S "ciao"]])
                     , ("field poly", I 42)
                     , ("field 1"   , I 42)
                     ]
                 )
               , ("field 1", I 14)
               ]
    it "should generically encode-decode T values"
        $ roundTripWorks
        $ T 14 (Just [["ciao"], ["mamma"]])
        $ i' 42
    it "should generically encode-decode T values with optionals"
        $ roundTripWorks
        $ T 14 Nothing
        $ i' 42
    





newtype ArbB = ArbB {arbB :: B} deriving Show

instance Arbitrary ArbB where
    arbitrary = ArbB <$> sized generateB

generateB :: Int -> Gen B
generateB n = oneof $ [S <$> arbitrary, I <$> arbitrary] <> case n of
    0 -> []
    _ ->
        [ L <$> listOf deep'
        , D . fromList <$> listOf ((,) <$> arbitrary <*> deep')
        ]
    where deep' = generateB (n `div` 1000000) -- scale max depth
