{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Main where

import Application.Main
import Client
import Control.Logging
import Options.Applicative.Simple
import Protolude hiding (option)
import Torrent.Taint

-- | Client Version
version :: [Char]
version = "0.0.1"

-- | Command line arguments
data Options = Options
  { options_torrent :: FilePath,
    options_port :: Int,
    options_chunk_size :: Int64,
    options_verbose :: Bool
  }
  deriving (Show)

main :: IO ()
main = withStdoutLogging do
  setLogLevel LevelWarn
  (Options {..}, ()) <- simpleOptions
    do version
    do "Header for command line arguments"
    do "Flood is a bittorrent client POC, developed by haskell meetup in Milan "
    do
      Options
        <$> strOption
          do long "file" <> short 'f' <> help "file path"
        <*> option
          auto
          do long "port" <> short 'p' <> value 6881 <> help "tcpPort"
        <*> option
          auto
          do long "size" <> short 's' <> value 16384 <> help "size of chunks"
        <*> switch
          do long "verbose" <> short 'v' <> help "Verbose output?"
    do empty
  if options_verbose then setLogLevel LevelDebug else pure ()
  clientId <- generatePeerId
  log' $ "Client ID is set to " <> show clientId
  let client = Client clientId "localhost" options_port $ PieceLength options_chunk_size
  id <- async $ downloadTorrentFile client options_torrent
  log' "Network for torrent started."
  link id
  void getLine
